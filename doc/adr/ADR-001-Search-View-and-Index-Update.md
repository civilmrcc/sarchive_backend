# ADR-001: Search View and Index Update

## Context

For the search, we decided to have a full-text search on the case number, the free text, and the engine status, as well as a field specific filter on date from and to, the boat color and the type, and finally the tags. 

For the fuzzy text search, we use a materialized view joining the `cases`, `boat_details`, and `tags` table.
The full-text search index indexes the fields `case_number`, `free_text`. Having all these tables joined in one view gives us the possibility to easily find all cases concerning all these criteria. The case Ids will then be used to create the case objects.

Materialized views do not update automatically, so the question is when the update is triggered. Options are:

* database trigger on changes
* dedicated refresh: `REFRESH MATERIALIZED VIEW CONCURRENTLY full_cases_mv;` after case update, triggered from the code

## Decision

We will use the dedicated refresh called from the case mutation handlers. For that we have a convenient class method in FullCaseForSearch:
```Python
FullCaseForSearch.update_materialized_view()
```

## Status

Accepted

## Consequences

With this approach, we are able to control, when the index is updated. 

Especially, when we would use triggers on all the tables mentioned above, this could potentially lead to quite some load on the database.
For example, if you think about mass upload from OneFleet to SARchive, we can update the view, after _all_ data are imported rather than trigger an update with each record. 

As a programmer, you need to be aware, that you have to trigger a view update after a change on the case. If this is forgotten, this might lead to missing cases in the search result.
