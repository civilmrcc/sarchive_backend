# Contributing

Contributions are welcome, and they are greatly appreciated! Every little bit
helps, and credit will always be given.

You can contribute in many ways:

## Types of Contributions

### Report Bugs

Report bugs at [https://github.com/domma/cases_backend/issues](https://github.com/domma/cases_backend/issues).

If you are reporting a bug, please include:

* Your operating system name and version.
* Any details about your local setup that might be helpful in troubleshooting.
* Detailed steps to reproduce the bug.

### Fix Bugs

Look through the GitHub issues for bugs. Anything tagged with "bug" and "help wanted" is open to whoever wants to implement it.

### Implement Features

Look through the GitHub issues for features. Anything tagged with "enhancement" and "feature" is open to whoever wants to implement it.

### Write Documentation

Cases Backend could always use more documentation, whether as part of the official Cases Backend docs, in docstrings, or even on the web in blog posts, articles, and such.

### Submit Feedback

The best way to send feedback is to file an issue at [https://github.com/domma/cases_backend/issues](https://github.com/domma/cases_backend/issues).

If you are proposing a feature:

* Explain in detail how it would work.
* Keep the scope as narrow as possible, to make it easier to implement.
* Remember that this is a volunteer-driven project, and that contributions
  are welcome :)

## Development Workflow

We use Kanban, where the issues on the frontend project (this one) define the next item(s) to work on, while the [frontend](https://gitlab.com/civilmrcc/sarchive_frontend) needs to follow.

Check this [board](https://gitlab.com/civilmrcc/sarchive_backend/-/boards) for an overview.

Important cornerstones of the workflow are:

- label issue with `To Do` if you want to raise it's priority and have a developer pick it "next"
- label issue as `Doing` and assign yourself when you are working on it, to avoid two people working on the same
- label issue with `frontend` if there is a dependency in the frontend project and link the frontend issue
- To be transparent about changes and deployment status:
  - work on issues in `feature/*` or `fix/` branches (see [GitFlow](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow))
  - label issue as `done`, when developments/changes are done and merged to the `develop` branch
  - close issues which are `done`, when the respective changes was merged from `develop` to `master`
    - this is done at an aligned point in time via merge request, by a maintainer of the repository/ies
  - The state of `master` will automatically be deployed to testing
    
### Architecture Decisions

We keep major architecture decisions in [Architecture Decision Records](./adr). 

See [Documenting Architecture Decisions](https://cognitect.com/blog/2011/11/15/documenting-architecture-decisions) for further details.

## Get Started!

Ready to contribute? Here's how to set up `cases_backend` for local development.

1. Fork the `cases_backend` repo on GitHub.
2. Clone your fork locally: `git clone git@github.com:your_name_here/cases_backend.git`

3. Setup your [local environment](https://gitlab.com/civilmrcc/sarchive_backend/tree/fix/implement_link#setup)

4. Create a branch for local development: `git checkout -b name-of-your-bugfix-or-feature`
    Now you can make your changes locally. 
    **To ensure code consistency, you must use [Black](https://black.readthedocs.io/en/stable/) for code formatting**

5. When you're done making changes, check that your changes pass flake8 and the
   tests. We have handy make tasks for this
    ```shell script
    $ make lint
    $ make test
    ```

6. Commit your changes and push your branch to GiLab:
    ```shell script
    $ git add .
    $ git commit -m "Your detailed description of your changes."
    $ git push origin name-of-your-bugfix-or-feature
    ```
   
7. Submit a pull request through the GitHub website.

## Pull Request Guidelines

Before you submit a pull request, check that it meets these guidelines:

1. The pull request should include tests.
2. If the pull request adds functionality, the docs should be updated. Put your new functionality into a function with a docstrig
3. The pull request should work for Python >=3.7

## Database Changes

We use Alembic (wrapped in flask-migrate) to track DB changes. If your development involves changes to the data model, you have to perform the following steps:

1. Make the necessary changes to the model classes
2. Create an automatic migration script with the `flask db migrate -m "<change message>""` command
3. Review the generated script and adjust it (if needed), so that it accurately represents the changes you made to the models
4. Add the migration script to git
5. Apply the migration to the database with the `flask db upgrade` command  

### Additional Fields
If you want to add a new field to the database, follow [this checklist](./checklist_new_db_field.md).

## Error Handling

Errors in the API are created by raising runtime errors in the Python code. The message pattern is the following:

`MESSAGE_CODE_ALL_CAPS: Some additional text`

The error codes must be declared in the `ErrorCodes` enum in the `main.graphql` schema definition.

## Tips

To run a subset of tests: `python -m unittest tests.test_cases_backend`

## Deploying

A reminder for the maintainers on how to deploy.
Make sure all your changes are committed (including an entry in HISTORY.md).
Then run
```shell script
$ poetry version <new_version> 
$ git push
$ git push --tags
```
When merging the develop branch to master GitLab's CI automatically starts the deployment.
