- [ ] Update the [Graph QL Interface definition](../cases_backend/graphql/main.graphql)
- [ ] Create a testcase. Maybe one of the [templates](../tests/test_case_templates) is useful
- [ ] Implement
    - [ ] [Model](../cases_backend/models.py) enhancement
    - [ ] Queries
    - [ ] Mutations
- [ ] Create an Alembic Database Migration
- [ ] Update the Testdata Generator if needed
- [ ] Update the [Changelog](../HISTORY.md)
