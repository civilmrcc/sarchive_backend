# History

## Next
### Shiny New Things
- Add Search Capabilities to API (cases query)
    - Freetext on case number, case notes, supporting the following search parameters
      - `&` AND
      - `|` OR
      - `!` NOT 
      - `<->` FOLLOWED BY
      - `:*` PREFIX SEARCH
    - Search on a list of tags, boat types and boat colors
    - Search on the case occurrence date with a range
- Pagination
- Frontex Involvement Field
- Case Outcome Field
- Case Status Field
- Case Estimated Departure Time Field
- Case Authorities Alerted Field
- Case Authorities Details Field
- Boat Details: Freetext Field
- Positions: SOG/COG Fields

### Fixes
- Remove `cases-by-tag` mutation from API

## 0.2.0 (2021-01-04)
- Remove the categories. Tags are now single entities assigned to a case
- Enable multiple links per case

## 0.1.0 (2019-10-19)
- First release on PyPI.
