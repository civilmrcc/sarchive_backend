FROM python:3

EXPOSE 5000

WORKDIR /cases_backend

ENV FLASK_APP app.py
ENV FLASK_RUN_HOST 0.0.0.0
ENV FLASK_ENV development
ENV FLASK_CONFIG development

COPY . .
RUN rm -rf .venv

RUN python3 -m pip install virtualenv
RUN python3 -m venv .venv
RUN python3 -m pip install --upgrade pip
ENV PATH=./.venv/bin:$PATH

RUN . .venv/bin/activate && pip install poetry
RUN . .venv/bin/activate && poetry install -v

CMD flask run
