import os

from ariadne import (
    MutationType,
    QueryType,
    load_schema_from_path,
    make_executable_schema,
)
from flask import Flask
from flask_bootstrap import Bootstrap
from flask_cors import CORS
from flask_login import LoginManager
from flask_moment import Moment
from flask_sqlalchemy import SQLAlchemy

from .config import config

here = os.path.split(__file__)[0]

db = SQLAlchemy()
login_manager = LoginManager()


def create_app(config_name):
    # ------------------------ Create Flask App ------------------------
    app = Flask(__name__)
    print(f"Loading '{config_name}' configuration")
    app.config.from_object(config[config_name])
    config[config_name].init_app(app)

    CORS(app, resources={r"*": {"origins": "*"}})

    db.init_app(app)

    # Configuration for the Admin Console
    Bootstrap(app)
    login_manager.login_view = "admin.login"
    login_manager.init_app(app)
    Moment(app)

    # Register blueprints
    from .main import main as main_blueprint
    from .admin import admin as admin_blueprint

    app.register_blueprint(admin_blueprint, url_prefix="/admin")
    app.register_blueprint(main_blueprint)

    # ----------------------- Initialize Ariadne -----------------------
    query = QueryType()
    app.config["ARIADNE_QUERY"] = query

    mutation = MutationType()
    app.config["ARIADNE_MUTATION"] = mutation

    # Import GraphQL modules
    app_ctx = app.app_context()
    app_ctx.push()

    import cases_backend.graphql.auth_mutations  # noqa: F401
    import cases_backend.graphql.case_mutations  # noqa: F401
    import cases_backend.graphql.case_queries  # noqa: F401
    import cases_backend.graphql.tag_queries  # noqa: F401
    import cases_backend.graphql.user_mutation  # noqa: F401
    import cases_backend.graphql.user_queries  # noqa: F401
    import cases_backend.graphql.settings_queries  # noqa: F401

    app_ctx.pop()

    type_defs = load_schema_from_path(os.path.join(here, "graphql/main.graphql"))
    schema = make_executable_schema(type_defs, [query, mutation])
    app.config["ARIADNE_SCHEMA"] = schema

    # ------------------------------ Done ------------------------------
    return app
