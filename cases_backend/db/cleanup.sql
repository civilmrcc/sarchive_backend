drop table if exists users;
drop table if exists cases;
drop table if exists people_on_board;
drop table if exists positions;
drop table if exists boat_details;
drop extension if exists postgis;

/* Tagging */

drop table if exists tag_categories;
drop table if exists tag_names;
drop table if exists tag_assignments;
