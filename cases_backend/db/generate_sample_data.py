from random import randint, uniform

from faker import Faker
from progress.bar import Bar

from cases_backend.models import (
    BoatDetails,
    Case,
    PeopleOnBoard,
    Position,
    Tag,
    Link,
)


def generate_sample_data(verbose, cases, db, silent=False):
    fake = Faker()

    # Tags
    tags = []

    for i in range(10):
        tags.append(Tag(name=fake.name()))

        db.session.add(tags[i])

        if verbose and not silent:
            print("Added: ", tags[i])

    # Cases
    # Show progress bar in non verbose mode
    if not verbose and not silent:
        bar = Bar("Create cases", max=cases)

    for i in range(cases):
        # people on board
        people_on_board = PeopleOnBoard(
            total=randint(5, 30),
            women=randint(5, 30),
            men=randint(5, 30),
            minors=randint(5, 30),
            medical=randint(5, 30),
            missing=randint(5, 30),
            drowned=randint(5, 30),
        )

        # boat details
        boat_details = BoatDetails(
            boat_type=fake.random_element(
                elements=("WOOD", "FIBERGLASS", "RUBBER", "OTHER")
            ),
            color=fake.random_element(
                elements=(
                    "RED",
                    "GREEN",
                    "BLUE",
                    "YELLOW",
                    "WHITE",
                    "BLACK",
                    "GRAY",
                    "BROWN",
                )
            ),
            engine_status=fake.text(max_nb_chars=75),
        )

        # case
        case = Case(case_number=str(i) + "_" + fake.name())
        case.occurred_at = fake.date_time_between(start_date="-2y")
        case.freetext = fake.text()
        case.people_on_board = people_on_board
        case.boat_details = boat_details

        # generate links
        for j in range(randint(0, 5)):
            case.links.append(Link(url=fake.uri()))

        # generate positions
        for j in range(randint(0, 10)):
            long = uniform(-180, 180)
            lat = uniform(-90, 90)
            case.positions.append(
                Position(
                    pos_id=j,
                    position=f"POINT({long} {lat})",
                    registered_at=fake.date_time_between(start_date="-2y"),
                )
            )

        # add some tags
        for j in range(randint(1, 4)):
            case.tags.append(tags[randint(0, 9)])

        db.session.add(case)
        db.session.commit()

        if not silent:
            if verbose:
                print("Added: ", case)
                print("\t", people_on_board)
                [print("\t", position) for position in case.positions]
                [print("\t", link) for link in case.links]
                [print("\t", tag) for tag in case.tags]
            else:
                bar.next()

    if not verbose and not silent:
        bar.finish()

    if not silent:
        print(cases, "test cases created")
