create or replace function trigger_set_timestamp ()
    returns trigger
    as $$
begin
    new.updated_at = now();
    return new;
end;
$$
language plpgsql;

create table users (
    id serial,
    login varchar(250),
    password varchar(250),
    active boolean
);

insert into users (login, password, active)
    values ('admin', '$pbkdf2-sha256$30000$NKZUinGOce6dU0qpVao1hg$tbWXtfEe/9JolYBjEMTbUSenFqZVgetPKBabAs2EnYo', true);

create table cases (
    id serial,
    case_number varchar(250) not null,
    freetext text,
    occurred_at timestamp,
    created_at timestamp not null default NOW(),
    updated_at timestamp not null default NOW()
);

create index idx_cases_case_number on cases (case_number);

create trigger set_timestamp
    before update on cases for each row
    execute procedure trigger_set_timestamp ();

create table people_on_board (
    case_id int,
    total int,
    women int,
    men int,
    minors int,
    medical int,
    missing int,
    drowned int,
    created_at timestamp not null default NOW(),
    updated_at timestamp not null default NOW()
);

create index idx_people_on_board_case_id on people_on_board (case_id);

create trigger people_on_board
    before update on people_on_board for each row
    execute procedure trigger_set_timestamp ();

create table positions (
    case_id int,
    pos_id int,
    pos geography (point),
    registered_at timestamp,
    created_at timestamp not null default NOW(),
    updated_at timestamp not null default NOW()
);

create unique index idx_positions_case_id on positions (case_id, pos_id);

create trigger set_timestamp
    before update on positions for each row
    execute procedure trigger_set_timestamp ();

create table boat_details (
    case_id int,
    boat_type varchar(100),
    color varchar(100),
    engine_status varchar(100),
    created_at timestamp not null default NOW(),
    updated_at timestamp not null default NOW()
);

create index idx_boat_details_case_id on boat_details (case_id);

create trigger set_timestamp
    before update on boat_details for each row
    execute procedure trigger_set_timestamp ();


/* Tagging */
create table tag_categories (
    id serial4,
    name varchar(200),
    description text,
    created_at timestamp not null default NOW(),
    updated_at timestamp not null default NOW()
);

insert into tag_categories (name, description)
    values ('others', 'automatically generated default category');

create unique index idx_category_name on tag_categories (name);

create trigger set_timestamp
    before update on tag_categories for each row
    execute procedure trigger_set_timestamp ();

create table tag_names (
    id serial4,
    category_id integer,
    name varchar(200),
    created_at timestamp not null default NOW(),
    updated_at timestamp not null default NOW(),
    constraint unique_category_id_name unique (category_id, name)
);

create index idx_tag_name on tag_names (name);

create index idx_tag_category_id on tag_names (category_id);

create unique index idx_tag_name_category_id on tag_names (category_id, name);

create trigger set_timestamp
    before update on tag_names for each row
    execute procedure trigger_set_timestamp ();

create table tag_assignments (
    case_id integer,
    tag_id integer,
    created_at timestamp not null default NOW(),
    updated_at timestamp not null default NOW(),
    constraint unique_case_id_tag_id unique (case_id, tag_id)
);

create index idx_tag_assignments_case_id on tag_assignments (case_id);

create index idx_tag_assignments_tag_id on tag_assignments (tag_id);

create trigger set_timestamp
    before update on tag_assignments for each row
    execute procedure trigger_set_timestamp ();
