drop database if exists caba;
create database caba;

\connect caba
create extension postgis;

drop user if exists caba;
create user caba with password 'caba';
grant all on database caba to caba;
