-- setup a dedicated db
create database sarchive;

-- ensure the postgis extension is enabled on the db server
create extension postgis;

-- create the sarchive application user
create user sarchive with ENCRYPTED password 'yourpass';

-- grant privileges to app user on created schema
grant select, insert, update, delete on all tables in schema public to sarchive;

grant usage, select on all sequences in schema public to sarchive;

