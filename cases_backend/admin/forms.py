from flask_wtf import FlaskForm
from wtforms import (
    StringField,
    PasswordField,
    SubmitField,
    BooleanField,
    Field,
)
from wtforms.validators import DataRequired, Length, EqualTo

from cases_backend.auth.role_model import Permission


class LoginForm(FlaskForm):
    username = StringField("Username*", validators=[DataRequired(), Length(1, 250)])
    password = PasswordField("Password*", validators=[DataRequired()])

    submit = SubmitField("Log In")


class ChangePasswordForm(FlaskForm):
    current_password = PasswordField("Current Password*", validators=[DataRequired()])
    new_password = PasswordField("New Password*", validators=[DataRequired()],)
    new_password_repeat = PasswordField(
        "Confirm New Password*",
        validators=[
            DataRequired(),
            EqualTo("new_password", message="Passwords must match"),
        ],
    )

    submit = SubmitField("Change Password")


def build_user_form(roles):
    """
    Dynamically create the user form with the roles generated from
    the database table. The output of the Roles label is a bit of a hack,
    but for now I don't have a better way to only return a info text
    """

    class TextLabelWidget(object):
        def __call__(self, field, **kwargs):
            return ""

    class TextLabel(Field):
        widget = TextLabelWidget()

        def _value(self):
            return self.data if self.data is not None else ""

    class CreateUserFormStatic(FlaskForm):
        username = StringField("Username*", validators=[DataRequired(), Length(1, 250)])
        password = PasswordField("Password*", validators=[DataRequired()])
        active = BooleanField("Active", default=True)

    class CreateUserForm(CreateUserFormStatic):
        pass

    setattr(CreateUserForm, "role_info", TextLabel(label="User Roles:"))

    if roles:
        for role in roles:
            setattr(
                CreateUserForm,
                role.name,
                BooleanField(
                    label=f"{role.description}",
                    default=(role.name == Permission.DEFAULT_ROLE),
                ),
            )

    setattr(CreateUserForm, "submit", SubmitField("Create User"))

    return CreateUserForm()


def build_edit_user_form(roles):
    """
    Dynamically create the user form with the roles generated from
    the database table. The output of the Roles label is a bit of a hack,
    but for now I don't have a better way to only return a info text
    """

    class TextLabelWidget(object):
        def __call__(self, field, **kwargs):
            return ""

    class TextLabel(Field):
        widget = TextLabelWidget()

        def _value(self):
            return self.data if self.data is not None else ""

    class EditUserFormStatic(FlaskForm):
        username = StringField("Username", render_kw={"readonly": True})
        active = BooleanField("Active", default=True)

    class EditUserForm(EditUserFormStatic):
        pass

    setattr(EditUserForm, "role_info", TextLabel(label="User Roles:"))

    if roles:
        for role in roles:
            setattr(
                EditUserForm, role.name, BooleanField(label=f"{role.description}"),
            )

    setattr(EditUserForm, "submit", SubmitField("Change User"))

    return EditUserForm()
