from flask import Blueprint, redirect, url_for, request, flash
from flask_login import current_user

from .. import login_manager
from ..auth import User


class MessageConstants:
    SUCCESS = "alert-success"
    ERROR = "alert-danger"
    WARNING = "alert-warning"
    INFO = "alert-info"


admin = Blueprint("admin", __name__, template_folder="templates")

from . import views  # noqa: F401, E402
from . import errors  # noqa: F401, E402
from . import forms  # noqa: F401, E402


@login_manager.user_loader
def load_user(user_id):
    current_user = User.query.filter_by(id=user_id).first()

    if not current_user:
        # In case a strange session cookie situation occurs
        return None

    if current_user.active is False:
        return None

    return current_user


@admin.before_app_request
def before_request():
    # Request a password change if needed
    if (
        request.endpoint != "admin.change_password"
        and request.endpoint != "admin.logout"
    ):
        if current_user.is_authenticated and current_user.is_password_initial:
            flash(
                "You need to change your password at the first login.",
                category=MessageConstants.WARNING,
            )
            return redirect(url_for("admin.change_password"))
