from flask import render_template, request, url_for, redirect, flash
from flask_login import login_user, login_required, logout_user, current_user
from flask_wtf import FlaskForm

from . import admin, MessageConstants
from .forms import LoginForm, ChangePasswordForm, build_user_form, build_edit_user_form
from .. import db
from ..auth import login_user as my_login_user, AuthorizationError, User
from ..auth.decorators import requires_permission_for_ui
from ..auth.role_model import Permission, Role


@admin.route("/", methods=["GET"])
@login_required
@requires_permission_for_ui(Permission.P_ADMIN_UI_OPEN)
def index():
    return render_template("admin/index.html")


@admin.route("/login", methods=["GET", "POST"])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        try:
            user = my_login_user(form.username.data, form.password.data)
            login_user(user)

            next = request.args.get("next")
            if next is None or not next.startswith("/"):
                next = url_for("admin.index")

            if user.is_password_initial:
                flash(
                    "You need to change your password at the first login.",
                    category=MessageConstants.WARNING,
                )
                next = url_for("admin.change_password")

            return redirect(next)
        except AuthorizationError as e:
            flash(str(e).split(":")[1], category=MessageConstants.ERROR)

    return render_template("admin/login.html", form=form)


@admin.route("/changepassword", methods=["GET", "POST"])
@login_required
@requires_permission_for_ui(Permission.P_ADMIN_UI_OPEN)
def change_password():
    form = ChangePasswordForm()

    if form.validate_on_submit():
        if not current_user.verify_password(form.current_password.data):
            flash(
                "The current password you provided is incorrect. Please try again.",
                category=MessageConstants.ERROR,
            )
            return render_template("admin/change_password.html", form=form)

        current_user.password = form.new_password.data
        db.session.commit()

        flash(
            "Your password was successfully changed.", category=MessageConstants.SUCCESS
        )
        return redirect(url_for("admin.index"))

    return render_template("admin/change_password.html", form=form)


@admin.route("/logout")
@login_required
def logout():
    logout_user()
    flash("You have been logged out.", category=MessageConstants.INFO)
    return redirect(url_for("admin.index"))


@admin.route("/createuser", methods=["GET", "POST"])
@login_required
@requires_permission_for_ui(Permission.P_ADMIN_UI_OPEN)
@requires_permission_for_ui(Permission.P_USER_MANAGE)
def create_user():
    roles = Role.query.all()
    form = build_user_form(roles)

    if form.validate_on_submit():
        exists = (
            db.session.query(User.id).filter_by(username=form.username.data).scalar()
        )
        if exists is not None:
            flash("User already exists.", category=MessageConstants.ERROR)
            return render_template("admin/create_user.html", form=form)

        new_user = User(
            username=form.username.data,
            password=form.password.data,
            active=form.active.data,
        )

        for role in roles:
            if form.data[role.name]:
                new_user.add_role(role.name)

        db.session.add(new_user)
        db.session.commit()

        flash("User successfully created.", category=MessageConstants.SUCCESS)
        return redirect(url_for("admin.create_user"))

    return render_template("admin/create_user.html", form=form)


@admin.route("/edituser", methods=["GET", "POST"])
@login_required
@requires_permission_for_ui(Permission.P_ADMIN_UI_OPEN)
@requires_permission_for_ui(Permission.P_USER_MANAGE)
def edit_user():
    # Check if the user exists
    username = request.form.get("username")

    if username is None:
        flash(
            "No username was provided. Please select a user in the list below.",
            category=MessageConstants.ERROR,
        )
        return redirect(url_for("admin.manage_users"))

    user = User.query.filter_by(username=username).first()

    if user is None:
        flash(f"User '{username}' not found.", category=MessageConstants.ERROR)
        return redirect(url_for("admin.manage_users"))

    roles = Role.query.all()
    form = build_edit_user_form(roles)

    if form.validate_on_submit() and "submit_button" not in request.form:
        # if the 'submit_button' is part of the form, then this is the
        # first passing when being called from the management page.
        # in this case, we don't want to process the submit
        user.remove_all_roles()

        user.active = form.active.data

        for role in roles:
            if form.data[role.name]:
                user.add_role(role.name)

        db.session.commit()

        flash("User successfully changed.", category=MessageConstants.SUCCESS)
        return render_template("admin/edit_user.html", form=form)

    form.username.data = username
    form.active.data = user.active

    for role in user.roles:
        form[role.name].data = True

    return render_template("admin/edit_user.html", form=form)


@admin.route("/manageusers", methods=["GET", "POST"])
@login_required
@requires_permission_for_ui(Permission.P_ADMIN_UI_OPEN)
@requires_permission_for_ui(Permission.P_USER_MANAGE)
def manage_users():
    users = User.query.order_by("username").all()

    # We use a POST Redirect to the edit page. Therefore the framework
    # needs a CSRF Token. Since we have no 'real' form on this page,
    # we create a dummy form to generate a CSRF Token, which will be
    # rendered into the page
    form = FlaskForm(meta={"csrf": True})

    if request.method == "POST":
        if request.form.get("submit_button") == "delete":
            # Can't delete logged in user
            if request.form.get("uname") == current_user.username:
                flash(
                    "Logged in user can't be deleted.", MessageConstants.ERROR,
                )
                return redirect(url_for("admin.manage_users"))

            user = User.query.filter_by(username=request.form.get("uname")).first()

            if user is None:
                flash(
                    f'Error deleting "{request.form.get("uname")}". If the issue persists, please contact the admin.',
                    MessageConstants.ERROR,
                )
                return redirect(url_for("admin.manage_users"))

            db.session.delete(user)
            db.session.commit()
            flash(
                '"' + request.form.get("uname") + '" deleted.', MessageConstants.SUCCESS
            )
            return redirect(url_for("admin.manage_users"))
        if request.form.get("submit_button") == "changePassword":
            # Can't change logged in user
            if request.form.get("uname") == current_user.username:
                flash(
                    "Logged in user can't be changed. Use the function in your profile section.",
                    MessageConstants.ERROR,
                )
                return redirect(url_for("admin.manage_users"))

            new_password = request.form.get("newPassword")

            if new_password is None:
                flash(
                    "You must provide a password.", MessageConstants.ERROR,
                )
                return redirect(url_for("admin.manage_users"))

            user = User.query.filter_by(username=request.form.get("uname")).first()

            if user is None:
                flash(
                    f'Error changing password for "{request.form.get("uname")}". If the issue persists, please contact the admin.',
                    MessageConstants.ERROR,
                )
                return redirect(url_for("admin.manage_users"))

            user.password = new_password
            db.session.commit()
            flash(
                'Password for "' + request.form.get("uname") + '" changed.',
                MessageConstants.SUCCESS,
            )
            return redirect(url_for("admin.manage_users"))
        elif request.form.get("submit_button") == "edit":
            return redirect(url_for("admin.edit_user"), code=307)
        else:
            pass

    return render_template("admin/manage_users.html", users=users, form=form)
