import os


class Config:
    APP_SECRET = os.environ.get("APP_SECRET") or "hard to guess string"
    SECRET_KEY = APP_SECRET  # Required by Flask-WTF
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    @staticmethod
    def init_app(app):
        pass


class DevelopmentConfig(Config):
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = (
        os.environ.get("SQLALCHEMY_DATABASE_URI") or "postgresql://caba:caba@localhost"
    )


class TestingConfig(Config):
    TESTING = True
    WTF_CSRF_ENABLED = False
    # Database is created per test dynamically with an own URL. This
    # is set in the tests/conftest.py client fixture


class ProductionConfig(Config):
    DEBUG = False
    TESTING = False
    SQLALCHEMY_DATABASE_URI = (
        os.environ.get("SQLALCHEMY_DATABASE_URI") or "postgresql://caba:caba@localhost"
    )


config = {
    "development": DevelopmentConfig,
    "testing": TestingConfig,
    "production": ProductionConfig,
    "default": DevelopmentConfig,
}
