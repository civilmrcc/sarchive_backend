from datetime import datetime

from flask import current_app

from ..auth.decorators import requires_authentication, requires_permission
from ..auth.role_model import Permission
from ..models import Case

query = current_app.config["ARIADNE_QUERY"]


class CaseNotFound(Exception):
    def __init__(self, case_number):
        self.case_number = case_number

    def __str__(self):
        return f"Case number '{self.case_number}' not found."


@requires_authentication
@query.field("case")
@requires_permission(Permission.P_CASE_READ)
def resolve_case(_, info, id):
    case = Case.query.filter_by(id=id).first()

    if case is None:
        raise CaseNotFound("CASE_NOT_FOUND:" + id)

    return convert_case_to_gql_structure(case)


@requires_authentication
@requires_permission(Permission.P_CASE_READ)
@query.field("cases")
def resolve_cases(_, info, filter=None, pagination=None):
    # Process and check pagination criteria
    if pagination:
        limit = pagination.get("limit", 20)
        offset = pagination.get("offset", 0)
    else:
        limit = 20
        offset = 0

    response_metadata = {
        "total": 0,
        "page": 0,
        "pages": 0,
        "pageSize": limit,
        "offset": offset,
    }

    # Process and check the filter criteria
    if filter:
        text = filter.get("text")
        boat_types = filter.get("boatTypes")
        boat_colors = filter.get("boatColors")
        tags = filter.get("tags")

        # Validate From Date
        case_date_from = filter.get("caseDateFrom")
        if case_date_from:
            try:
                datetime.strptime(case_date_from, "%Y-%m-%d %H:%M:%S")
            except ValueError:
                raise RuntimeError(f"INVALID_DATE_FORMAT: {case_date_from}")

        # Validate To Date
        case_date_to = filter.get("caseDateTo")
        if case_date_to:
            try:
                datetime.strptime(case_date_to, "%Y-%m-%d %H:%M:%S")
            except ValueError:
                raise RuntimeError(f"INVALID_DATE_FORMAT: {case_date_from}")

        cases, response_metadata["total"] = Case.get_filtered_and_paged_case_list(
            text,
            tags,
            case_date_from,
            case_date_to,
            boat_types,
            boat_colors,
            limit,
            offset,
        )
    else:
        cases = Case.query.order_by(Case.created_at).limit(limit).offset(offset).all()

        response_metadata["total"] = Case.query.count()

    ret_cases = []
    for case in cases:
        ret_cases.append(convert_case_to_gql_structure(case))

    # Calculate the pagination metadata
    response_metadata["pages"] = (
        response_metadata["total"] // response_metadata["pageSize"]
    )

    # In case there is a rest, this goes to an additional page
    if response_metadata["total"] % response_metadata["pageSize"] != 0:
        response_metadata["pages"] = response_metadata["pages"] + 1

    response_metadata["page"] = offset // limit + 1

    return {
        "cases": ret_cases,
        "responseMetadata": response_metadata,
    }


# ------------------------------- Helper -------------------------------
def convert_case_to_gql_structure(case: Case):
    ret_case = {}

    ret_case["id"] = case.id
    ret_case["caseNumber"] = case.case_number
    ret_case["freetext"] = case.freetext
    ret_case["timestamp"] = case.occurred_at
    ret_case["frontexInvolvement"] = case.frontex_involvement
    ret_case["outcome"] = case.outcome
    ret_case["status"] = case.status
    ret_case["estimatedDepartureTime"] = case.estimated_departure_time
    ret_case["authoritiesAlerted"] = case.authorities_alerted
    ret_case["authoritiesDetails"] = case.authorities_details

    ret_case["peopleOnBoard"] = case.people_on_board

    ret_case["boat"] = {}
    if case.boat_details:
        ret_case["boat"]["color"] = case.boat_details.color
        ret_case["boat"]["engineStatus"] = case.boat_details.engine_status
        ret_case["boat"]["type"] = case.boat_details.boat_type
        ret_case["boat"]["freetext"] = case.boat_details.freetext

    ret_case["positions"] = []
    for position in case.positions:
        ret_position = {}
        ret_position["id"] = position.pos_id
        ret_position["caseId"] = position.case_id
        ret_position["timestamp"] = position.registered_at

        ret_position["speedOverGround"] = position.speed_over_ground
        ret_position["courseOverGround"] = position.course_over_ground

        ret_position["longitude"] = position.longitude
        ret_position["latitude"] = position.latitude

        ret_case["positions"].append(ret_position)

    ret_case["tags"] = case.tags

    ret_case["links"] = [link.url for link in case.links]

    return ret_case
