from flask import g, current_app

from cases_backend import db
from cases_backend.auth import AuthorizationError, UserError
from cases_backend.auth.decorators import requires_authentication

mutation = current_app.config["ARIADNE_MUTATION"]


@requires_authentication
@mutation.field("changePassword")
def resolve_change_password(_, info, oldPassword, newPassword) -> bool:
    """
    Change the password for a give user.
    If the current password is incorrect, an exception is thrown
    If the current password is equals to the new password, an exception
    is thrown
    :param _: object from parent resolver
    :param info: context object for query
    :type info: GraphQLResolverInfo
    :param username: user name
    :type username: str
    :param oldPassword: old password
    :type oldPassword: str
    :param newPassword: new password
    :type newPassword: str
    :return: was the change successful
    :rtype: bool
    """
    if oldPassword == newPassword:
        raise UserError(
            "USER_PWD_CANT_BE_SAME_AS_CURRENT: New password can't be equal to current password"
        )

    if not g.current_user.verify_password(oldPassword):
        raise AuthorizationError("AUTH_WRONG_PASSWORD: Wrong password")

    g.current_user.password = newPassword

    db.session.commit()

    return True
