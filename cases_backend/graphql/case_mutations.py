from flask import current_app

from .. import db
from ..auth.role_model import Permission
from ..models import (
    BoatDetails,
    Case,
    PeopleOnBoard,
    Position,
    Tag,
    Link,
    FullCaseForSearch,
)
from ..auth.decorators import requires_authentication, requires_permission
from .case_queries import resolve_case

mutation = current_app.config["ARIADNE_MUTATION"]


@requires_authentication
@requires_permission(Permission.P_CASE_CREATE)
@mutation.field("createCase")
def resolve_create_case(_, info, case):
    case_obj = Case(
        case_number=case["caseNumber"],
        freetext=case.get("freetext"),
        occurred_at=case.get("timestamp"),
        frontex_involvement=case.get("frontexInvolvement"),
        outcome=case.get("outcome"),
        status=case.get("status"),
        estimated_departure_time=case.get("estimatedDepartureTime"),
        authorities_alerted=case.get("authoritiesAlerted"),
        authorities_details=case.get("authoritiesDetails"),
    )

    # Links
    links = case.get("links")

    if links:
        for link in links:
            case_obj.links.append(Link(url=link))

    # Boat Details
    boat = case.get("boat")
    if boat:
        case_obj.boat_details = BoatDetails(
            boat_type=boat.get("type"),
            color=boat.get("color"),
            engine_status=boat.get("engineStatus"),
            freetext=boat.get("freetext"),
        )

    # Position
    position = case.get("position")
    if position:
        case_obj.positions.append(
            Position.get_position_from_gql_struct(
                position_struct=position, case_id=case_obj.id
            )
        )

    # People On Board
    people_on_board = case.get("peopleOnBoard")
    if people_on_board:
        case_obj.people_on_board = PeopleOnBoard(
            total=people_on_board.get("total"),
            women=people_on_board.get("women"),
            men=people_on_board.get("men"),
            minors=people_on_board.get("minors"),
            medical=people_on_board.get("medical"),
            missing=people_on_board.get("missing"),
            drowned=people_on_board.get("drowned"),
        )

    # Tags
    tags = case.get("tags")

    if tags:
        for tag in tags:
            tag_obj = Tag.query.filter_by(name=tag["name"]).first()

            # Tags will be created, if they do not exist yet
            if tag_obj is None:
                tag_obj = Tag(name=tag["name"], description=tag.get("description"))

            case_obj.tags.append(tag_obj)

    db.session.add(case_obj)
    db.session.commit()
    FullCaseForSearch.update_materialized_view()

    # use the resolver to read the new case from the DB
    return resolve_case(None, None, case_obj.id)


@requires_authentication
@requires_permission(Permission.P_CASE_UPDATE)
@mutation.field("updateCase")
def resolve_update_case(_, info, case):
    case_obj = Case.query.filter_by(id=case["id"]).first()  # type:Case

    if case_obj is None:
        raise RuntimeError(f"CASE_NOT_FOUND: Case {case['id']} does not exist")

    case_obj.case_number = case.get("caseNumber", case_obj.case_number)
    case_obj.freetext = case.get("freetext", case_obj.freetext)
    case_obj.occurred_at = case.get("timestamp", case_obj.occurred_at)
    case_obj.frontex_involvement = case.get(
        "frontexInvolvement", case_obj.frontex_involvement
    )
    case_obj.outcome = case.get("outcome", case_obj.outcome)
    case_obj.status = case.get("status", case_obj.status)
    case_obj.estimated_departure_time = case.get(
        "estimatedDepartureTime", case_obj.occurred_at
    )
    case_obj.authorities_alerted = case.get(
        "authoritiesAlerted", case_obj.authorities_alerted
    )
    case_obj.authorities_details = case.get(
        "authoritiesDetails", case_obj.authorities_details
    )

    boat = case.get("boat")
    if boat:
        case_obj.boat_details.boat_type = boat.get(
            "type", case_obj.boat_details.boat_type
        )
        case_obj.boat_details.color = boat.get("color", case_obj.boat_details.color)
        case_obj.boat_details.engine_status = boat.get(
            "engineStatus", case_obj.boat_details.engine_status
        )
        case_obj.boat_details.freetext = boat.get(
            "freetext", case_obj.boat_details.freetext
        )

    people_on_board = case.get("peopleOnBoard")
    if people_on_board:
        case_obj.people_on_board.total = people_on_board.get(
            "total", case_obj.people_on_board.total
        )
        case_obj.people_on_board.women = people_on_board.get(
            "women", case_obj.people_on_board.women
        )
        case_obj.people_on_board.men = people_on_board.get(
            "men", case_obj.people_on_board.men
        )
        case_obj.people_on_board.minors = people_on_board.get(
            "minors", case_obj.people_on_board.minors
        )
        case_obj.people_on_board.medical = people_on_board.get(
            "medical", case_obj.people_on_board.medical
        )
        case_obj.people_on_board.missing = people_on_board.get(
            "missing", case_obj.people_on_board.missing
        )
        case_obj.people_on_board.drowned = people_on_board.get(
            "drowned", case_obj.people_on_board.missing
        )

    tags = case.get("tags")

    # To ensure also removed tags are considered, we remove all
    # tag assignments for the case and re-assign everything we
    # receive from the client. Tags themselves are kept.
    # But only if tags are provided at all
    if tags is not None:
        case_obj.tags = []

        for tag in tags:
            tag_obj = Tag.query.filter_by(name=tag["name"]).first()

            # Tags will be created, if they do not exist yet
            if tag_obj is None:
                tag_obj = Tag(name=tag["name"])

            case_obj.tags.append(tag_obj)

    # Links
    # To ensure also removed links are considered, we remove all
    # links for the case and re-assign everything we
    # receive from the client.
    links = case.get("links")

    if links is not None:
        case_obj.links = []
        # Also Remove from Database
        db.session.execute(Link.__table__.delete().where(Link.case_id == case_obj.id))

        for link in links:
            case_obj.links.append(Link(url=link))

    db.session.commit()
    FullCaseForSearch.update_materialized_view()

    # use the resolver to read the new case from the DB
    return resolve_case(None, None, case_obj.id)


@requires_authentication
@requires_permission(Permission.P_CASE_UPDATE)
@mutation.field("addPositionToCase")
def resolve_add_position_to_case(_, info, position, caseId):
    case_obj = Case.query.filter_by(id=caseId).first()

    if case_obj is None:
        raise RuntimeError(f"CASE_NOT_FOUND: Case {caseId} does not exist")

    position = Position.get_position_from_gql_struct(
        position_struct=position, case_id=case_obj.id
    )

    case_obj.positions.append(position)

    db.session.commit()

    return {
        "id": position.pos_id,
        "caseId": position.case_id,
        "latitude": position.latitude,
        "longitude": position.longitude,
        "timestamp": position.registered_at,
        "speedOverGround": position.speed_over_ground,
        "courseOverGround": position.course_over_ground,
    }


@requires_authentication
@requires_permission(Permission.P_CASE_UPDATE)
@mutation.field("deletePositionFromCase")
def resolve_delete_position_from_case(_, info, id, caseId):
    position = Position.query.filter_by(case_id=caseId, pos_id=id).first()

    if position is None:
        return False

    db.session.delete(position)
    db.session.commit()

    return True
