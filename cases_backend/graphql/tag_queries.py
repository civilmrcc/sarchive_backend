from flask import current_app

from ..auth.decorators import requires_authentication
from ..models import Tag

query = current_app.config["ARIADNE_QUERY"]


@requires_authentication
@query.field("tags")
def resolve_categories(_, info):
    return Tag.query.all()
