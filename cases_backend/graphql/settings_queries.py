from flask import current_app

query = current_app.config["ARIADNE_QUERY"]


@query.field("appSettings")
def resolve_categories(_, info):
    """
    This resolver currently does not return any payload, only a structure.
    The frontend uses the ErrorCodes enum type of the
    schema definition do generate an array of error codes.
    """
    return {"appSettings": {"errorCodes": None}}
