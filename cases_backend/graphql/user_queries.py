from flask import current_app, g

from cases_backend.auth.decorators import requires_authentication

query = current_app.config["ARIADNE_QUERY"]


@requires_authentication
@query.field("me")
def resolve_categories(_, info):
    user = g.current_user

    return {
        "id": user.id,
        "username": user.username,
        "isActive": user.active,
        "isPasswordInitial": user.is_password_initial,
        "roles": user.roles,
    }
