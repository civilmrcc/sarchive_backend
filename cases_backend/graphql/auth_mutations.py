from flask import current_app

from ..auth import AuthorizationError, login_user

mutation = current_app.config["ARIADNE_MUTATION"]


@mutation.field("login")
def resolve_login(_, info, username, password):
    user = login_user(username, password)

    token = user.create_token()

    if token:
        return {
            "token": token,
            "user": {
                "id": user.id,
                "username": user.username,
                "isActive": user.active,
                "isPasswordInitial": user.is_password_initial,
                "roles": user.roles,
            },
        }

    # If all goes south
    raise AuthorizationError(
        "AUTH_LOGON_FAILED: Login failed: Could not authenticate user."
    )
