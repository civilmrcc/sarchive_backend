from functools import wraps

from flask import abort
from flask_login import current_user

from cases_backend.auth.role_model import Role


def requires_authentication(func):
    """
    Adds the `_requires_authentication` attribute to the function to be
    decorated. This attribute is evaluated in the authorization middlerware
    :func:`cases_backend.auth.authorization_middleware`
    """
    setattr(func, "_requires_authentication", True)
    return func


def requires_permission(permission):
    """
    Adds the `_requires_permission` attribute to the function to be
    decorated. This attribute is evaluated in the authorization middlerware
    :func:`cases_backend.auth.authorization_middleware`

    The method expects a valid permission object:

    - type: `dict`
    - has a property with the name `value`
    - property type is `int`

    See :class:`cases_backend.auth.role_model.Permission`

    :raises AssertionError: in case of invalid permission
    :param permission: permission object
    :type permission: dict
    """

    def decorator_permission(func):
        Role.check_permission_parameter(permission)
        setattr(func, "_requires_permission", permission)
        return func

    return decorator_permission


def requires_permission_for_ui(permission):
    """
    Checks if the user has the requested permission. If not, Flask
    responses with 403 error code

    The method expects a valid permission object:

    - type: `dict`
    - has a property with the name `value`
    - property type is `int`

    See :class:`cases_backend.auth.role_model.Permission`

    :raises AssertionError: in case of invalid permission
    :param permission: permission object
    :type permission: dict
    """

    def decorator_permission(func):
        @wraps(func)
        def decorated_function(*args, **kwargs):
            Role.check_permission_parameter(permission)
            if not current_user.has_permission(permission):
                abort(403)

            return func(*args, **kwargs)

        return decorated_function

    return decorator_permission
