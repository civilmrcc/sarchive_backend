from datetime import datetime, timedelta

import jwt
import ulid
from flask import current_app
from flask_login import UserMixin
from werkzeug.security import generate_password_hash, check_password_hash

from cases_backend import db
from cases_backend.auth.role_model import Role, Permission
from cases_backend.models import BaseModel


class User(UserMixin, BaseModel, db.Model):
    __tablename__ = "users"
    id = db.Column(db.String(26), primary_key=True)
    username = db.Column(db.String(250), index=True, nullable=False, unique=True)
    password_hash = db.Column(db.String(250), index=False, nullable=False)
    roles = db.relationship(
        "Role",
        secondary="user_role_assignments",
        lazy="immediate",
        backref=db.backref(
            "users", lazy="dynamic", cascade="all, delete", single_parent=True,
        ),
    )
    active = db.Column(db.Boolean, server_default="TRUE")
    is_password_initial = db.Column(db.Boolean, server_default="TRUE")

    @staticmethod
    def insert_default_user():
        admin = User.query.filter_by(username="admin").first()

        if admin is None:
            admin = User(
                username="admin",
                password="admin",
                active=True,
                is_password_initial=True,
            )
            db.session.add(admin)

        admin.add_role(Permission.R_ADMIN)

        db.session.commit()

    @staticmethod
    def get_user_by_token(token):
        app_secret = current_app.config["APP_SECRET"]
        token_content = jwt.decode(token, app_secret, algorithms=["HS256"], verify=True)

        if "sub" not in token_content:
            raise RuntimeError("AUTH_TOKEN_INVALID: Invalid Token")

        current_user = User.query.filter_by(username=token_content["sub"]).first()

        if current_user is None:
            raise RuntimeError(
                f"AUTH_USER_NOT_FOUND: User {token_content['sub']} not found"
            )

        if current_user.active is False:
            raise RuntimeError(
                f"AUTH_USER_NOT_ACTIVE: User {token_content['sub']} is inactive"
            )

        return current_user

    def __init__(self, username, password, is_password_initial=True, **kwargs):
        super(User, self).__init__(**kwargs)

        if "id" not in kwargs:
            self.id = ulid.from_timestamp(datetime.utcnow()).str

        self.username = username
        self.password = password
        self.is_password_initial = is_password_initial

    def __repr__(self):
        return "User(id='{}', username='{}', active={}, is_password_initial={})".format(
            self.id, self.username, self.active, self.is_password_initial
        )

    @property
    def password(self):
        raise AttributeError("Password is not a readable attribute")

    @password.setter
    def password(self, password):
        self.password_hash = generate_password_hash(password)
        self.is_password_initial = False

    @property
    def is_active(self):
        """
        Overwrites the login mixin, which always returns true
        :return: active state of the user
        :rtype: bool
        """
        return self.active

    def verify_password(self, password):
        return check_password_hash(self.password_hash, password)

    def create_token(self):
        app_secret = current_app.config["APP_SECRET"]

        payload = {
            "exp": datetime.utcnow() + timedelta(days=0, seconds=3600),
            "iat": datetime.utcnow(),
            "sub": self.username,
        }

        return jwt.encode(payload, app_secret, algorithm="HS256").decode("utf-8")

    def add_role(self, name):
        """
        Adds a role to the user. Returns True, if the assignment was successful.
        If the role was already assigned, or the role does not exist in the
        database, false is returned.

        :param name: Role name
        :type name: str
        :return: Successful
        :rtype: bool
        """
        if not any(role.name == name for role in self.roles):
            db_role = Role.query.filter_by(name=name).first()
            if db_role is None:
                return False
            else:
                self.roles.append(db_role)
                return True

        return False

    def remove_role(self, name):
        """
        Removes a role from the user. Returns true, if successful, otherwise
        False
        :param name: Role name
        :type name: str
        :return: Successful
        :rtype: bool
        """
        if not any(role.name == name for role in self.roles):
            return False
        else:
            self.roles = [role for role in self.roles if role.name != name]
            return True

    def remove_all_roles(self):
        """
        Removes all roles from the user
        :rtype: bool
        """
        for role in self.roles:
            self.remove_role(role.name)

    def has_permission(self, permission):
        """
        Checks if a role contains a permission.
        The method expects a valid permission
        object:

        - type: `dict`
        - has a property with the name `value`
        - property type is `int`

        See :class:`cases_backend.auth.role_model.Permission`

        :raises AssertionError: in case of invalid permission
        :param permission: permission object
        :type permission: dict
        :return: check result
        :rtype: bool
        """
        Role.check_permission_parameter(permission)

        has_permission = False
        for role in self.roles:
            if role.has_permission(permission):
                has_permission = True
                break

        return has_permission
