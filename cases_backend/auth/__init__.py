from flask import g
from jwt import ExpiredSignatureError

from .. import db
from .user_model import User


class PasswordInitialError(Exception):
    pass


class AuthorizationError(Exception):
    pass


class UserError(Exception):
    pass


def parse_and_validate_request(request):
    """
    Checks the request for an authentication token and loads the respective
    user.

    :raise: RuntimeError, AuthorizationError
    :param request: Flask request object
    :type request:
    """
    auth_header = request.headers.get("Authorization")
    if not auth_header:
        raise AuthorizationError("AUTH_TOKEN_MISSING: Missing authorization token.")

    try:
        g.current_user = User.get_user_by_token(auth_header[7:])
    except ExpiredSignatureError:
        raise AuthorizationError(
            "AUTH_TOKEN_EXPIRED: The authorization token timed out."
        )


def authorization_middleware(resolver, obj, info, **args):
    # Check for authentication
    requires_authentication = getattr(resolver, "_requires_authentication", None)
    if requires_authentication is not None:
        request = info.context
        parse_and_validate_request(request)

        # If the password is initial, it needs to be changed after login
        # all other action than changePassword will lead to an error in
        # this case
        if (
            g.current_user.is_password_initial is True
            and info.field_name != "changePassword"  # allow the changePassword mutation
        ):
            raise PasswordInitialError(
                "AUTH_PASSWORD_NEEDS_CHANGE: Password needs to be changed at first login"
            )

    # Check for authorization
    required_permission = getattr(resolver, "_requires_permission", None)
    if required_permission is not None:
        if not g.current_user.has_permission(required_permission):
            raise AuthorizationError(
                "AUTH_MISSING_PERMISSION: " + required_permission["description"]
            )

    return resolver(obj, info, **args)


def login_user(username, password):
    user = User.query.filter_by(username=username).first()

    if user is None:
        raise AuthorizationError("AUTH_USER_NOT_FOUND: User not found")
    if not user.active:
        raise AuthorizationError("AUTH_USER_NOT_ACTIVE: User not active")
    if not user.verify_password(password):
        raise AuthorizationError(
            "AUTH_NOT_AUTHENTICATED: Not Authorized. Wrong username or password."
        )

    return user


def create_user(username, password, active):
    exists = db.session.query(User.id).filter_by(username=username).scalar()
    if exists is not None:
        raise UserError("USER_ALREADY_EXISTS: User already exists")

    new_user = User(username=username, password=password, active=active)

    db.session.add(new_user)
    db.session.commit()
    return new_user
