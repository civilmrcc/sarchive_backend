from datetime import datetime

import ulid

from cases_backend import db
from cases_backend.models import BaseModel


class Permission:
    # Valid Permissions
    P_CASE_READ = {"value": 1, "description": "Read Case"}
    P_CASE_CREATE = {"value": 2, "description": "Create Case"}
    P_CASE_UPDATE = {"value": 4, "description": "Update Case"}

    P_CATEGORY_CREATE = {"value": 8, "description": "Create Category"}
    P_CATEGORY_UPDATE = {"value": 16, "description": "Update Category"}
    P_CATEGORY_DELETE = {"value": 32, "description": "Delete Category"}

    P_USER_MANAGE = {"value": 64, "description": "Manage Users"}

    P_LOG_VIEW = {"value": 128, "description": "View Change Log"}

    P_ADMIN_UI_OPEN = {"value": 256, "description": "Use the admin web interface"}

    # Valid Role Names
    R_CASE_RESEARCHER = "CaseResearcher"
    R_CASE_MANAGER = "CaseManager"
    R_CATEGORY_MANAGER = "CategoryManager"
    R_ADMIN = "Admin"

    # Role ↔ Permission assignment
    ROLES = {
        "CaseResearcher": {
            "permissions": ["P_CASE_READ"],
            "description": "Case researcher (read cases)",
        },
        "CaseManager": {
            "permissions": [
                "P_CASE_READ",
                "P_CASE_CREATE",
                "P_CASE_UPDATE",
                "P_LOG_VIEW",
            ],
            "description": "Case manager (read, create and update cases)",
        },
        "CategoryManager": {
            "permissions": [
                "P_CATEGORY_CREATE",
                "P_CATEGORY_UPDATE",
                "P_CATEGORY_DELETE",
            ],
            "description": "Category manager (create, update and delete categories)",
        },
        "Admin": {
            "permissions": [
                "P_CASE_READ",
                "P_CASE_CREATE",
                "P_CASE_UPDATE",
                "P_CATEGORY_CREATE",
                "P_CATEGORY_UPDATE",
                "P_CATEGORY_DELETE",
                "P_LOG_VIEW",
                "P_USER_MANAGE",
                "P_ADMIN_UI_OPEN",
            ],
            "description": "Administrator (administer cases, categories and users)",
        },
    }

    DEFAULT_ROLE = R_CASE_RESEARCHER


class Role(db.Model, BaseModel):
    __tablename__ = "roles"

    id = db.Column(db.String(26), primary_key=True)
    name = db.Column(db.String(64), unique=True, index=True)
    default = db.Column(db.Boolean, default=False, index=True)
    permissions = db.Column(db.Integer)
    description = db.Column(db.String)

    @staticmethod
    def insert_default_roles():
        for role in Permission.ROLES:
            db_role = Role.query.filter_by(name=role).first()

            if db_role is None:
                db_role = Role(name=role)

            db_role.reset_permissions()
            db_role.default = role == Permission.DEFAULT_ROLE
            for permission_name in Permission.ROLES[role]["permissions"]:
                db_role.add_permission(getattr(Permission, permission_name))

            db_role.description = Permission.ROLES[role]["description"]

            db.session.add(db_role)

        db.session.commit()

    @staticmethod
    def check_permission_parameter(permission):
        """
        Checks if a permission object is valid:

        - type: `dict`
        - has a property with the name `value`
        - property type is `int`

        See :class:`cases_backend.auth.role_model.Permission`

        :raises AssertionError: in case of invalid permission
        :param permission: permission object
        :type permission: dict
        """
        assert type(permission) == dict
        assert "value" in permission
        assert type(permission["value"]) == int

    @staticmethod
    def get_role_by_name(role_name: str) -> "Role":
        return Role.query.filter_by(name=role_name).first()

    def add_permission(self, permission):
        """
        Adds a permission to the role. The method expects a valid permission
        object:

        - type: `dict`
        - has a property with the name `value`
        - property type is `int`

        See :class:`cases_backend.auth.role_model.Permission`

        :raises AssertionError: in case of invalid permission
        :param permission: permission object
        :type permission: dict
        """
        self.check_permission_parameter(permission)

        if not self.has_permission(permission):
            self.permissions += permission["value"]

    def remove_permission(self, permission):
        """
        Removes a permission from the role. The method expects a valid
        permission object:

        - type: `dict`
        - has a property with the name `value`
        - property type is `int`

        See :class:`cases_backend.auth.role_model.Permission`

        :raises AssertionError: in case of invalid permission
        :param permission: permission object
        :type permission: dict
        """
        self.check_permission_parameter(permission)

        if self.has_permission(permission):
            self.permissions -= permission["value"]

    def reset_permissions(self):
        self.permissions = 0

    def has_permission(self, permission):
        """
        Checks for a permission in the role. The method expects a valid
        permission object:

        - type: `dict`
        - has a property with the name `value`
        - property type is `int`

        See :class:`cases_backend.auth.role_model.Permission`

        :raises AssertionError: in case of invalid permission
        :param permission: permission object
        :type permission: dict
        :return: check result
        :rtype: bool
        """
        self.check_permission_parameter(permission)

        return self.permissions & permission["value"] == permission["value"]

    def __init__(self, **kwargs):
        super(Role, self).__init__(**kwargs)

        if "id" not in kwargs:
            self.id = ulid.from_timestamp(datetime.utcnow()).str

        if self.permissions is None:
            self.permissions = 0

    def __repr__(self, **kwargs):
        return "Role(id='{}', name='{}', default={}, permissions={})".format(
            self.id, self.name, self.default, self.permissions
        )


tag_assignments = db.Table(
    "user_role_assignments",
    db.Column("user_id", db.String(26), db.ForeignKey("users.id"), primary_key=True),
    db.Column("role_id", db.String(26), db.ForeignKey("roles.id"), primary_key=True),
)
