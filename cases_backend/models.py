from datetime import datetime

import ulid
from flask import current_app
from geoalchemy2 import Geometry
from sqlalchemy import func, literal_column
from sqlalchemy.dialects import postgresql
from sqlalchemy.ext.automap import automap_base
from sqlalchemy.ext.declarative import declarative_base

from migrations.versions.c419894589e8_materialized_view_for_search import index_columns
from . import db


class BaseModel(object):
    created_at = db.Column(db.DateTime(), server_default=func.now())
    updated_at = db.Column(
        db.DateTime(), server_default=func.now(), onupdate=func.now()
    )


# -------------------------------- Case --------------------------------
class Case(BaseModel, db.Model):
    __tablename__ = "cases"
    id = db.Column(db.String(26), primary_key=True)
    case_number = db.Column(db.String(250), index=True, nullable=False)
    freetext = db.Column(db.Text(), nullable=False, default="")
    links = db.relationship(
        "Link",
        backref="case",
        cascade="all, delete, delete-orphan",
        order_by="Link.url",
    )
    occurred_at = db.Column(db.DateTime())
    estimated_departure_time = db.Column(db.DateTime())
    frontex_involvement = db.Column(db.String(10))
    outcome = db.Column(db.String(50))
    status = db.Column(db.String(50))
    authorities_alerted = db.Column(db.Boolean(), default=False)
    authorities_details = db.Column(db.String(), default="")
    boat_details = db.relationship("BoatDetails", backref="case", uselist=False)
    people_on_board = db.relationship("PeopleOnBoard", backref="case", uselist=False)
    positions = db.relationship(
        "Position", backref="case", order_by="Position.registered_at"
    )
    tags = db.relationship(
        "Tag",
        secondary="tag_assignments",
        lazy="dynamic",
        backref=db.backref("cases", lazy="dynamic"),
    )

    def __init__(self, **kwargs):
        super(Case, self).__init__(**kwargs)

        if "id" not in kwargs:
            self.id = ulid.from_timestamp(datetime.utcnow()).str

    def __repr__(self):
        return "Case(id='{}', case_number='{}')".format(
            self.id,
            self.case_number,
        )

    @classmethod
    def get_filtered_and_paged_case_list(
        cls,
        text: str = None,
        tags: list = None,
        case_date_from: datetime = None,
        case_date_to: datetime = None,
        boat_types: list = None,
        boat_colors: list = None,
        limit: int = 20,
        offset: int = 0,
    ) -> tuple:
        """
        Factory method, that returns a list of case Objects based on
        the filter and the paging parameter provided

        :return: Tuple with a list of cases according to the paging and the
                 total number of matches

        """

        matching_cases, total = FullCaseForSearch.filter(
            text,
            tags,
            case_date_from,
            case_date_to,
            boat_types,
            boat_colors,
            limit,
            offset,
        )

        cases = (
            Case.query.filter(Case.id.in_(matching_cases))
            .order_by(Case.created_at)
            .all()
        )

        return cases, total


# ------------------------------- Links --------------------------------
class Link(BaseModel, db.Model):
    __tablename__ = "links"
    case_id = db.Column(db.String(26), db.ForeignKey("cases.id"), primary_key=True)
    url = db.Column(db.String, primary_key=True)

    def __repr__(self):
        return "Link(case_id={}, url={}".format(
            self.case_id,
            self.url,
        )


# --------------------------- PeopleOnBoard ----------------------------
class PeopleOnBoard(BaseModel, db.Model):
    __tablename__ = "people_on_board"
    case_id = db.Column(db.String(26), db.ForeignKey("cases.id"), primary_key=True)
    total = db.Column(db.SmallInteger)
    women = db.Column(db.SmallInteger)
    men = db.Column(db.SmallInteger)
    minors = db.Column(db.SmallInteger)
    medical = db.Column(db.SmallInteger)
    missing = db.Column(db.SmallInteger)
    drowned = db.Column(db.SmallInteger)

    def __repr__(self):
        return (
            "PeopleOnBoard(case_id='{}', total={}, women={}, men={}, minors={},"
            "medical={}, missing={}, drowned={})".format(
                self.case_id,
                self.total,
                self.women,
                self.men,
                self.minors,
                self.medical,
                self.missing,
                self.drowned,
            )
        )


# ------------------------------ Position ------------------------------
class Position(BaseModel, db.Model):
    __tablename__ = "positions"
    case_id = db.Column(db.String(26), db.ForeignKey("cases.id"), primary_key=True)
    pos_id = db.Column(db.SmallInteger, primary_key=True)
    position = db.Column(Geometry("POINT"))
    registered_at = db.Column(db.DateTime())
    speed_over_ground = db.Column(db.Float(asdecimal=True))
    course_over_ground = db.Column(db.Float(asdecimal=True))

    def __repr__(self):
        return "Position(case_id={}, pos_id={}, position={}, registered_at={}".format(
            self.case_id,
            self.pos_id,
            self.position,
            self.registered_at,
        )

    @staticmethod
    def get_position_from_gql_struct(position_struct, case_id):
        # Position ID's are incremented per case. Therefore we can't use a
        # sequence
        last_pos_id = (
            db.session.query(func.max(Position.pos_id))
            .filter_by(case_id=case_id)
            .first()
        )[0]

        pos_id = 0 if last_pos_id is None else last_pos_id + 1

        position = Position(
            case_id=case_id,
            pos_id=pos_id,
            position=f"POINT({position_struct.get('longitude')} {position_struct.get('latitude')})",
            registered_at=position_struct.get("timestamp"),
            speed_over_ground=position_struct.get("speedOverGround"),
            course_over_ground=position_struct.get("courseOverGround"),
        )

        return position

    @property
    def latitude(self):
        return db.session.scalar(self.position.ST_Y())

    @property
    def longitude(self):
        return db.session.scalar(self.position.ST_X())


# ---------------------------- BoatDetails -----------------------------
class BoatDetails(BaseModel, db.Model):
    __tablename__ = "boat_details"
    case_id = db.Column(db.String(26), db.ForeignKey("cases.id"), primary_key=True)
    boat_type = db.Column(db.String(100))
    color = db.Column(db.String(100))
    engine_status = db.Column(db.String(100))
    freetext = db.Column(db.Text(), nullable=True, default="")

    def __repr__(self):
        return "BoatDetails(case_id='{}', boat_type='{}', color='{}', engine_status='{}".format(
            self.case_id, self.boat_type, self.color, self.engine_status
        )


# -------------------------------- Tag ---------------------------------
class Tag(BaseModel, db.Model):
    __tablename__ = "tags"
    id = db.Column(db.String(26), primary_key=True)
    name = db.Column(db.String(100), index=True, nullable=False)

    def __init__(self, **kwargs):
        super(Tag, self).__init__(**kwargs)

        if "id" not in kwargs:
            self.id = ulid.from_timestamp(datetime.utcnow()).str

    def __repr__(self):
        return "Tag(id='{}', name='{}'".format(self.id, self.name)


tag_assignments = db.Table(
    "tag_assignments",
    db.Column("case_id", db.String(26), db.ForeignKey("cases.id"), primary_key=True),
    db.Column("tag_id", db.String(26), db.ForeignKey("tags.id"), primary_key=True),
)


# ------------------------- Complete Case For Search --------------------------
DeclarativeModel = declarative_base(cls=(db.Model))  # Needed to add query method
Base = automap_base(DeclarativeModel)


class FullCaseForSearch(Base):
    __tablename__ = "full_cases_mv"
    # id and tag are the virtual primary key for the table
    id = db.Column(db.String(26), primary_key=True)
    tag = db.Column(db.String(100), primary_key=True)
    case_number = db.Column(db.String(250))
    freetext = db.Column(db.Text())
    occurred_at = db.Column(db.DateTime())
    boat_type = db.Column(db.String(100))
    color = db.Column(db.String(100))
    engine_status = db.Column(db.String(100))

    def __repr__(self):
        return (
            f"FullCaseForSearch(id={self.id}, case_number={self.case_number}, "
            f"tag={self.tag}, occured_at={self.occurred_at}, "
            f"boat_type={self.boat_type}, color={self.color})"
        )

    @classmethod
    def update_materialized_view(cls):
        db.engine.execute("REFRESH MATERIALIZED VIEW CONCURRENTLY full_cases_mv;")

    @classmethod
    def filter(
        cls,
        text: str = None,
        tags: list = None,
        case_date_from: datetime = None,
        case_date_to: datetime = None,
        boat_types: list = None,
        boat_colors: list = None,
        limit: int = 20,
        offset: int = 0,
    ):
        """
        Reads a list of cases from the materialized view, that match the filter
        criteria

        :param text:
        :param tags:
        :param case_date_from:
        :param case_date_to:
        :param boat_types:
        :param boat_colors:
        :param limit:
        :param offset:
        :return: Tuple with a list of case Ids according to the paging and the total number of matches
        """
        query = db.session.query(cls.id)

        # Add the free text to the query if provided
        if text:
            query = query.filter(
                func.to_tsvector(
                    "english",
                    literal_column(index_columns),
                ).match(text, postgresql_regconfig="english")
            )

        if tags:
            tag_list = [tag["name"] for tag in tags]
            query = query.filter(cls.tag.in_(tag_list))

        if boat_types:
            query = query.filter(cls.boat_type.in_(boat_types))

        if boat_colors:
            query = query.filter(cls.color.in_(boat_colors))

        if case_date_from or case_date_to:
            select_date_from = "1900-01-01 00:00:00"
            select_date_to = "9999-12-31 00:00:00"

            # Only from date
            if case_date_from and not case_date_to:
                select_date_from = case_date_from

            # Only to date
            if case_date_to and not case_date_from:
                select_date_to = case_date_to

            # Both dates
            if case_date_to and case_date_from:
                select_date_from = case_date_from
                select_date_to = case_date_to

            query = query.filter(
                cls.occurred_at >= select_date_from, cls.occurred_at <= select_date_to
            )

        query = query.distinct()

        total = query.count()  # get the count bevor we limit to pages

        query = query.limit(limit).offset(offset)

        # Print the compiled query to the database and the analyze plan for potential
        # performance issues. We first check if debug mode is on, to avoid unnecessary
        # statement compilations
        if current_app.debug:
            current_app.logger.debug("Search - Raw SQL")
            current_app.logger.debug(
                str(
                    query.statement.compile(
                        dialect=postgresql.dialect(),
                        compile_kwargs={"literal_binds": True},
                    )
                ).replace("\n", " "),
            )

            current_app.logger.debug("Search - Execution Plan")
            execution_plan = db.engine.execute(
                "EXPLAIN ANALYZE "
                + str(
                    query.statement.compile(
                        dialect=postgresql.dialect(),
                        compile_kwargs={"literal_binds": True},
                    )
                )
            ).fetchall()

            for row in execution_plan:
                current_app.logger.debug(row)

        return query.all(), total


FullCaseForSearch.prepare()
