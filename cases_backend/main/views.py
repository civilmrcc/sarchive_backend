from ariadne import graphql_sync
from ariadne.constants import PLAYGROUND_HTML
from flask import current_app, request
from graphql.execution import MiddlewareManager

from cases_backend.auth import authorization_middleware

from . import main


@main.route("/", methods=["GET"])
def graphql_playgroud():
    return PLAYGROUND_HTML, 200


@main.route("/", methods=["POST"])
def graphql_server():

    data = request.get_json()
    success, result = graphql_sync(
        current_app.config["ARIADNE_SCHEMA"],
        data,
        context_value=request,
        debug=current_app.debug,
        middleware=MiddlewareManager(authorization_middleware),
    )

    status_code = 200 if success else 400
    return result, status_code
