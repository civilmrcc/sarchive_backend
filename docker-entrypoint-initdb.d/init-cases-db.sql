create database caba;
create user caba with password 'caba';
grant all on database caba to caba;

\connect caba
create extension postgis;
