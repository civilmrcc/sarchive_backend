VENV_NAME?=.venv
VENV_ACTIVATE=. $(VENV_NAME)/bin/activate
PYTHON=${VENV_NAME}/bin/python

.PHONY: all clean db sleep db-reset web refresh setup-local-db-conn install serve run test

all:
	docker-compose up --build --detach
	sleep 3
	# Initialize the database
	docker-compose run web flask initialize-db
	# Delete one-off containers
	docker-compose rm -f
	docker-compose logs -f

clean:
	docker-compose down

db:
	docker-compose up --build -d db

sleep:
	sleep 3

db-reset:
	flask initialize-db

db-testdata:
	flask create-sample-data --init-db

web:
	docker-compose up -d web
	docker-compose logs -f web

refresh: | clean db sleep db-reset all

setup-local-db-conn:
	export SQLALCHEMY_DATABASE_URI=postgresql://caba:caba@localhost:5432

install:
	${VENV_ACTIVATE} && poetry install

serve:
	flask run

run: | setup-local-db-conn db sleep install db-reset serve

test:
	pytest --cov=cases_backend

lint:
	flake8 --ignore=E501,W503 cases_backend
	flake8 --ignore=E501,W503 tests
