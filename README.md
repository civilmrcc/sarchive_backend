# SARchive - Search And Rescue Archive Backend Project
## Architecture
![](doc/media/SARchiveBackendArchitecture.png)

## Features

Features are driven from the frontend repository's issues: https://gitlab.com/civilmrcc/sarchive_frontend/-/issues

## How to run the Backend

### Setup

#### Environment

After you have cloned this repository, you have to install it. You should use a fresh virtual environment. The `venv` package is part of Python, but missing on some Linux systems. In that case, you have to install `venv` first and then run:

```bash
    python3 -m venv .venv
    source .venv/bin/activate
```

For package management we use [Poetry](https://python-poetry.org/). To install the dependencies, Poetry needs to be installed first. You can do that by using

```bash
curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python
```

Using `pip` to install `poetry` is also possible.

```bash
pip install --user poetry
```

For further information check out the [Poetry documentation](https://python-poetry.org/docs/#installation)

#### Dependencies

To install all the dependencies simply type

```bash
poetry install
```

If you don't need the development dependencies (e.g. you only need the backend for frontend development),

```bash
poetry install --no-dev
```

is sufficient.

### Run Backend

To run the backend, you have several options. The easiest is to run the database and the webserver in docker containers We have wrapped the setup in a `Makefile` for you. Alternatively, you can run the webserver on your own and connect the application to local database or one running in a container. All options are described below.

#### All in Docker on your Local Machine

This runs a webserver and a database in a docker container. you can use this setup if you need a backend for frontend development or just want to play with the builtin GraphQL Explorer at [http://localhost:5000](http://localhost:5000). This setup contains some sample data in the database.

```bash
make all
```

#### Just the Database in Docker on your local Machine

If you'd like to run the Python coding from your shell/dev environment, you still need a database. To encapsulate one in a docker container run

```bash
make db
make db-testdata
```

To run the server simply use `make serve` (make sure you activated your virtual environment)

#### Run everything in your Local Environment

After the setup, you have to create a development database. Assuming you are on Linux/MacOs with the latest Postgresql and PostGIS installed, you can create a development database executing the `./cases_backend/db/create-dev-db.sql` script.

```bash
sudo -u postgres psql < ./cases_backend/db/create-dev-db.sql
```

The script has created a database called `caba`, having as user `caba` with
password `caba`. You can now initialize the database (you have to make sure, that the `SQLALCHEMY_DATABASE_URI` env. variable points to the database)

```bash
export SQLALCHEMY_DATABASE_URI=postgresql://caba:caba@localhost:5432
make db-testdata
```

### CLI

All the relevant maintenance tasks are integrated in the `flask` cli. Each command has a short help which you can reach with the `--help` flag.

- `flask create-sample-data`: Create sample cases with fake data. Number of records can be specified.
- `flask create-user`: Create an active user in the database (needs an admin account)
- `flask initialize-db`: Initialize the database.
- `flask run`: Run a development server.

In production we use [https://gunicorn.org](https://gunicorn.org).
The production command line can be found in the `Procfile` in the root of this repository.

## Environment Variables

The behavior of the backend can be influenced by the following environment variables:

* `FLASK_CONFIG`: Determines which configuration will be loaded from the `config.py`. Default is `development`. Possible values are `development`, `testing`, `production`. In production mode there is no stack trace returned with the response to the client.
* `SQLALCHEMY_DATABASE_URI`: Specify the Postgresql URI in the form `postgresql://<username>:<password>@<host>:<port>`. Defaults to `postgresql://caba:caba@localhost`.

## Sample Requests

Use playground to send these: [http://localhost:5000](http://localhost:5000)

First, you need an authentication token:

```graphql
mutation {
  login(username: "admin", password: "admin")
}
```

Add this in the http-headers. In the playground set:

```json
{
  "Authorization": "Bearer <token>"
}
```

Then you can run a query for all cases:

```graphql
query {
  cases {
    id
    caseNumber
    positions {
      id
      caseId
      timestamp
      longitude
      latitude
    }
    peopleOnBoard {
      women
      men
      minors
      total
      medical
      missing
      drowned
    }
    boat {
      type
      color
      engineStatus
    }
    links
    freetext
    categories {
      id
      name
      description
      tags {
        id
        name
      }
    }
    timestamp
  }
}
```

## How to run tests

Install this project with development dependencies:

```bash
poetry install
```

The tests are using [https://pytest-pgsql.readthedocs.io/en/latest/](https://pytest-pgsql.readthedocs.io/en/latest/) which is doing some Postgresql magic. You don't need a running server, but a local
Postgresql installation. Run the tests via:

```bash
make test
```

No running instance of the service is needed as the tests are using build in test functionality of Flask.

## How to run against another DB

To run against the central development database on Heroku, export the database URL as `SQLALCHEMY_DATABASE_URI` environment variable.

## How to setup a new database

Refer to the `./cases_backend/db/setup-beta-db.sql` script. This creates a new database on a postgres server, installs the postgis extension and grants privileges to the user to run the application.

## How to contribute

See [contribution guide](./doc/contributing.md)
