# IMPORTANT: This test case is prefixed with zz, so that it's the last in
# the last in the test. Otherwise it screws up the test-environment
# We need to further investigate, why this happens
from cases_backend import db
from cases_backend.db.generate_sample_data import generate_sample_data
from cases_backend.models import FullCaseForSearch, Case

cases_query_sufix = """
  {
    cases{id}
    responseMetadata{
      total
      page
      pages
      pageSize
      offset
    }
  }
  """


def test_generator(client):
    generate_sample_data(verbose=True, cases=10, db=db, silent=False)

    generate_sample_data(verbose=False, cases=10, db=db, silent=False)

    generate_sample_data(verbose=True, cases=10, db=db, silent=True)


def test_paging_no_data(client, run_gql, login):
    generate_sample_data(verbose=True, cases=33, db=db, silent=True)
    FullCaseForSearch.update_materialized_view()

    assert Case.query.count() == 33

    token = login("admin", "admin")
    cases_query = "cases " + cases_query_sufix
    result = run_gql(query=cases_query, token=token)["data"]["cases"]

    assert len(result["cases"]) == 20

    response_metadata = result["responseMetadata"]
    assert response_metadata["total"] == 33
    assert response_metadata["page"] == 1
    assert response_metadata["pages"] == 2
    assert response_metadata["pageSize"] == 20
    assert response_metadata["offset"] == 0


def test_paging_no_data_page_size_multiplier_of_cases(client, run_gql, login):
    generate_sample_data(verbose=True, cases=40, db=db, silent=True)
    FullCaseForSearch.update_materialized_view()

    assert Case.query.count() == 40

    token = login("admin", "admin")
    cases_query = "cases " + cases_query_sufix
    result = run_gql(query=cases_query, token=token)["data"]["cases"]

    assert len(result["cases"]) == 20

    response_metadata = result["responseMetadata"]
    assert response_metadata["total"] == 40
    assert response_metadata["page"] == 1
    assert response_metadata["pages"] == 2
    assert response_metadata["pageSize"] == 20
    assert response_metadata["offset"] == 0


def test_paging_without_filter(client, run_gql, login):
    generate_sample_data(verbose=True, cases=43, db=db, silent=True)
    FullCaseForSearch.update_materialized_view()

    assert Case.query.count() == 43

    token = login("admin", "admin")

    # 4 Full Pages
    for i in range(4):
        cases_query = (
            f"cases(pagination:{{offset:{i*10} ,limit:10}}) " + cases_query_sufix
        )
        result = run_gql(query=cases_query, token=token)["data"]["cases"]

        assert len(result["cases"]) == 10

        response_metadata = result["responseMetadata"]
        assert response_metadata["total"] == 43
        assert response_metadata["page"] == i + 1
        assert response_metadata["pages"] == 5
        assert response_metadata["pageSize"] == 10
        assert response_metadata["offset"] == i * 10

    # 5th page
    cases_query = "cases(pagination:{offset:40 ,limit:10}) " + cases_query_sufix
    result = run_gql(query=cases_query, token=token)["data"]["cases"]

    assert len(result["cases"]) == 3

    response_metadata = result["responseMetadata"]
    assert response_metadata["total"] == 43
    assert response_metadata["page"] == 5
    assert response_metadata["pages"] == 5
    assert response_metadata["pageSize"] == 10
    assert response_metadata["offset"] == 40

    # 6th page which does not exist
    cases_query = "cases(pagination:{offset:50 ,limit:10}) " + cases_query_sufix
    result = run_gql(query=cases_query, token=token)["data"]["cases"]

    assert len(result["cases"]) == 0

    response_metadata = result["responseMetadata"]
    assert response_metadata["total"] == 43
    assert response_metadata["page"] == 6
    assert response_metadata["pages"] == 5
    assert response_metadata["pageSize"] == 10
    assert response_metadata["offset"] == 50


# For the testing with filter we use date_from 1900 so, that even the
# filtered record returns all entries. We're only interested in the paging
# logic here
def test_paging_with_filter(client, run_gql, login):
    generate_sample_data(verbose=True, cases=43, db=db, silent=True)
    FullCaseForSearch.update_materialized_view()

    assert Case.query.count() == 43

    token = login("admin", "admin")

    # 4 Full Pages
    for i in range(4):
        cases_query = (
            f'cases(filter: {{caseDateFrom: "1900-01-01 00:00:00"}}, pagination:{{offset:{i*10} ,limit:10}}) '
            + cases_query_sufix
        )
        result = run_gql(query=cases_query, token=token)["data"]["cases"]

        assert len(result["cases"]) == 10

        response_metadata = result["responseMetadata"]
        assert response_metadata["total"] == 43
        assert response_metadata["page"] == i + 1
        assert response_metadata["pages"] == 5
        assert response_metadata["pageSize"] == 10
        assert response_metadata["offset"] == i * 10

    # 5th page
    cases_query = (
        'cases(filter: {caseDateFrom: "1900-01-01 00:00:00"}, pagination:{offset:40 ,limit:10}) '
        + cases_query_sufix
    )
    result = run_gql(query=cases_query, token=token)["data"]["cases"]

    assert len(result["cases"]) == 3

    response_metadata = result["responseMetadata"]
    assert response_metadata["total"] == 43
    assert response_metadata["page"] == 5
    assert response_metadata["pages"] == 5
    assert response_metadata["pageSize"] == 10
    assert response_metadata["offset"] == 40

    # 6th page which does not exist
    cases_query = (
        'cases(filter: {caseDateFrom: "1900-01-01 00:00:00"}, pagination:{offset:50 ,limit:10}) '
        + cases_query_sufix
    )
    result = run_gql(query=cases_query, token=token)["data"]["cases"]

    assert len(result["cases"]) == 0

    response_metadata = result["responseMetadata"]
    assert response_metadata["total"] == 43
    assert response_metadata["page"] == 6
    assert response_metadata["pages"] == 5
    assert response_metadata["pageSize"] == 10
    assert response_metadata["offset"] == 50
