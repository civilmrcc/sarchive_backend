from cases_backend import db
from cases_backend.models import (
    BoatDetails,
    Case,
    PeopleOnBoard,
    Position,
    Tag,
    Link,
)


def generate_full_test_data():
    # Tags
    tag_1 = Tag(name="other")
    tag_2 = Tag(name="another")
    tag_3 = Tag(name="test tag")

    db.session.add_all([tag_1, tag_2, tag_3])

    # Case 1
    case_1 = Case(
        case_number="case_1",
        freetext="Test Case 1",
        occurred_at="2020-01-01 11:17:55",
        boat_details=BoatDetails(
            boat_type="RUBBER", color="RED", engine_status="Running"
        ),
        people_on_board=PeopleOnBoard(
            total=27, women=2, men=3, minors=4, medical=5, missing=6, drowned=7
        ),
    )

    case_1.positions.append(
        Position(
            pos_id=1, position="POINT(-10 10)", registered_at="2020-01-02 11:17:55"
        )
    )
    case_1.positions.append(
        Position(
            pos_id=2, position="POINT(-11 11)", registered_at="2020-01-03 11:17:55"
        )
    )
    case_1.positions.append(
        Position(
            pos_id=3, position="POINT(-12 12)", registered_at="2020-01-04 11:17:55"
        )
    )
    case_1.positions.append(
        Position(
            pos_id=4, position="POINT(-13 13)", registered_at="2020-01-05 11:17:55"
        )
    )

    case_1.tags = [tag_1, tag_3]

    case_1.links.append(Link(url="http://www.test.com"))

    # Case 2
    case_2 = Case(
        case_number="case_2",
        freetext="Test Case 2",
        occurred_at="2020-02-01 11:17:55",
        boat_details=BoatDetails(
            boat_type="WOOD", color="BLACK", engine_status="Broken"
        ),
        people_on_board=PeopleOnBoard(
            total=21, women=6, men=5, minors=4, medical=3, missing=2, drowned=1
        ),
    )

    case_2.positions.append(
        Position(
            pos_id=1, position="POINT(-14 14)", registered_at="2020-01-06 11:17:55"
        )
    )
    case_2.positions.append(
        Position(
            pos_id=2, position="POINT(-15 15)", registered_at="2020-01-07 11:17:55"
        )
    )
    case_2.positions.append(
        Position(
            pos_id=3, position="POINT(-16 16)", registered_at="2020-01-08 11:17:55"
        )
    )
    case_2.positions.append(
        Position(
            pos_id=4, position="POINT(-17 17)", registered_at="2020-01-09 11:17:55"
        )
    )

    case_2.tags = [tag_2]

    case_2.links.append(Link(url="http://www.test.com"))

    case_2.links.append(Link(url="http://www.test2.com"))

    # Case 3
    case_3 = Case(
        case_number="case_3",
        freetext="Test Case 3 - Minimal Test Case",
        occurred_at="2020-03-01 11:17:55",
    )
    db.session.add_all([case_1, case_2, case_3])

    db.session.commit()

    return case_1, case_2, case_3
