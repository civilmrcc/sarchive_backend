from cases_backend import db
from cases_backend.auth import User


def test_403(client):
    user = User(username="tester", password="test")
    user.is_password_initial = False
    db.session.add(user)
    db.session.commit()

    response = client.post(
        "/admin/login",
        data={"username": "tester", "password": "test"},
        follow_redirects=True,
    )

    assert response.status_code == 403
    assert (
        "You are not authorized to access this page. Please make sure, that you log in with an admin user."
        in response.get_data(as_text=True)
    )


def test_404(client):
    response = client.get("/admin/somepage",)

    assert response.status_code == 404
    assert (
        "Ooops. This page was not found. Are you sure you have the right URL?"
        in response.get_data(as_text=True)
    )
