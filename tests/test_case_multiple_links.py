from cases_backend.models import Case, Link

from .test_data.generate_test_data import generate_full_test_data

case_query = """
  case(id: "<id>") {
    id
    caseNumber
    links
  }
"""


def test_case_query_with_one_link(run_gql, login, postgresql_db):
    case_1, case_2, case_3 = generate_full_test_data()
    token = login("admin", "admin")

    query = case_query.replace("<id>", case_1.id)
    result = run_gql(query=query, token=token)

    case = result["data"]["case"]

    # General Case Data
    assert case["id"] == case_1.id
    assert case["caseNumber"] == "case_1"
    assert len(case["links"]) == 1
    assert case["links"][0] == "http://www.test.com"


def test_case_query_with_two_links(run_gql, login, postgresql_db):
    case_1, case_2, case_3 = generate_full_test_data()
    token = login("admin", "admin")

    query = case_query.replace("<id>", case_2.id)
    result = run_gql(query=query, token=token)

    case = result["data"]["case"]

    # General Case Data
    assert case["id"] == case_2.id
    assert case["caseNumber"] == "case_2"
    assert len(case["links"]) == 2
    assert case["links"][0] == "http://www.test.com"
    assert case["links"][1] == "http://www.test2.com"


def test_case_query_without_links(run_gql, login, postgresql_db):
    case_1, case_2, case_3 = generate_full_test_data()
    token = login("admin", "admin")

    query = case_query.replace("<id>", case_3.id)
    result = run_gql(query=query, token=token)

    case = result["data"]["case"]

    # General Case Data
    assert case["id"] == case_3.id
    assert case["caseNumber"] == "case_3"
    assert len(case["links"]) == 0


def test_create_case_with_one_link(run_gql, login, postgresql_db):
    token = login("admin", "admin")

    result = run_gql(
        mutation="""
        createCase(case: {
            caseNumber: "test_case"
            links: ["https://www.test.de"]
        }) {
           id
           links
        }""",
        token=token,
    )["data"]

    assert len(result["createCase"]["links"]) == 1
    assert result["createCase"]["links"][0] == "https://www.test.de"

    case = Case.query.filter_by(id=result["createCase"]["id"]).first()

    assert case is not None
    assert len(case.links) == 1
    assert case.links[0].url == "https://www.test.de"


def test_create_case_with_multiple_links(run_gql, login, postgresql_db):
    token = login("admin", "admin")

    result = run_gql(
        mutation="""
        createCase(case: {
            caseNumber: "test_case"
            links: ["https://www.test.de", "https://www.test2.de"]
        }) {
           id
           links
        }""",
        token=token,
    )["data"]

    assert len(result["createCase"]["links"]) == 2
    assert result["createCase"]["links"][0] == "https://www.test.de"
    assert result["createCase"]["links"][1] == "https://www.test2.de"

    case = Case.query.filter_by(id=result["createCase"]["id"]).first()

    assert case is not None
    assert len(case.links) == 2
    assert case.links[0].url == "https://www.test.de"
    assert case.links[1].url == "https://www.test2.de"


def test_update_case_links(run_gql, login, postgresql_db):
    case_obj = generate_full_test_data()[1]  # case_2
    token = login("admin", "admin")

    result = run_gql(
        mutation="""
            updateCase(
                case: {
                    links: ["https://www.new-test.com"]
                    id: "%s"
                }
            ) {
                id
                links
            }"""
        % case_obj.id,
        token=token,
    )

    assert len(result["data"]["updateCase"]["links"]) == 1
    assert result["data"]["updateCase"]["links"][0] == "https://www.new-test.com"

    case = Case.query.filter_by(id=case_obj.id).first()
    assert case is not None
    assert len(case.links) == 1
    assert case.links[0].url == "https://www.new-test.com"

    # Check that links are removed from the db-table
    links = Link.query.filter_by(case_id=case_obj.id).all()
    assert len(links) == 1


def test_remove_case_links(run_gql, login, postgresql_db):
    case_obj = generate_full_test_data()[1]  # case_2
    token = login("admin", "admin")

    result = run_gql(
        mutation="""
            updateCase(
                case: {
                    links: []
                    id: "%s"
                }
            ) {
                id
                links
            }"""
        % case_obj.id,
        token=token,
    )

    assert len(result["data"]["updateCase"]["links"]) == 0
    assert result["data"]["updateCase"]["links"] == []

    case = Case.query.filter_by(id=case_obj.id).first()
    assert case is not None
    assert len(case.links) == 0

    # Check that links are removed from the db-table
    links = Link.query.filter_by(case_id=case_obj.id).all()
    assert len(links) == 0
