from cases_backend.auth import User


def test_manage_user_get(client, login_ui):
    login_ui("admin", "admin")

    response = client.get("/admin/manageusers")
    assert response.status_code == 200
    assert "<title>SARchive - Manage Users</title>" in response.get_data(as_text=True)


def test_delete_user(client, login_ui):
    login_ui("admin", "admin")

    client.post(
        "/admin/createuser",
        data={"username": "tester", "password": "tester", "active": "True"},
        follow_redirects=True,
    )

    response = client.post(
        "/admin/manageusers",
        data={"uname": "tester", "submit_button": "delete"},
        follow_redirects=True,
    )
    assert response.status_code == 200
    assert "&#34;tester&#34; deleted." in response.get_data(as_text=True)


def test_delete_user_wo_parameter(client, login_ui):
    login_ui("admin", "admin")

    client.post(
        "/admin/createuser",
        data={"username": "tester", "password": "tester", "active": "True"},
        follow_redirects=True,
    )

    response = client.post(
        "/admin/manageusers", data={"submit_button": "delete"}, follow_redirects=True,
    )
    # print(response.get_data(as_text=True))
    assert response.status_code == 200
    assert "Error deleting &#34;None&#34;." in response.get_data(as_text=True)


def test_delete_invalid_user(client, login_ui):
    login_ui("admin", "admin")

    response = client.post(
        "/admin/manageusers",
        data={"uname": "tester", "submit_button": "delete"},
        follow_redirects=True,
    )
    assert response.status_code == 200
    assert "Error deleting &#34;tester&#34;." in response.get_data(as_text=True)


def test_delete_logged_in_user(client, login_ui):
    login_ui("admin", "admin")

    response = client.post(
        "/admin/manageusers",
        data={"uname": "admin", "submit_button": "delete"},
        follow_redirects=True,
    )
    assert response.status_code == 200
    assert "Logged in user can&#39;t be deleted." in response.get_data(as_text=True)


def test_change_password(client, login_ui):
    login_ui("admin", "admin")

    client.post(
        "/admin/createuser",
        data={"username": "tester", "password": "tester", "active": "True"},
        follow_redirects=True,
    )

    response = client.post(
        "/admin/manageusers",
        data={
            "newPassword": "new_password",
            "submit_button": "changePassword",
            "uname": "tester",
        },
        follow_redirects=True,
    )
    assert response.status_code == 200
    assert "Password for &#34;tester&#34; changed." in response.get_data(as_text=True)

    db_user = User.query.filter_by(username="tester").first()

    assert db_user.verify_password("new_password")


def test_change_password_wo_parameter(client, login_ui):
    login_ui("admin", "admin")

    client.post(
        "/admin/createuser",
        data={"username": "tester", "password": "tester", "active": "True"},
        follow_redirects=True,
    )

    response = client.post(
        "/admin/manageusers",
        data={"submit_button": "changePassword"},
        follow_redirects=True,
    )
    assert response.status_code == 200
    assert "You must provide a password." in response.get_data(as_text=True)


def test_change_password_wo_username(client, login_ui):
    login_ui("admin", "admin")

    client.post(
        "/admin/createuser",
        data={"username": "tester", "password": "tester", "active": "True"},
        follow_redirects=True,
    )

    response = client.post(
        "/admin/manageusers",
        data={"newPassword": "new_password", "submit_button": "changePassword"},
        follow_redirects=True,
    )

    assert response.status_code == 200
    assert "Error changing password for &#34;None&#34;." in response.get_data(
        as_text=True
    )


def test_change_password_for_invalid_user(client, login_ui):
    login_ui("admin", "admin")

    response = client.post(
        "/admin/manageusers",
        data={
            "uname": "tester",
            "submit_button": "changePassword",
            "newPassword": "new_password",
        },
        follow_redirects=True,
    )

    assert response.status_code == 200
    assert "Error changing password for &#34;tester&#34;." in response.get_data(
        as_text=True
    )


def test_change_password_for_logged_in_user(client, login_ui):
    login_ui("admin", "admin")

    response = client.post(
        "/admin/manageusers",
        data={"uname": "admin", "submit_button": "changePassword"},
        follow_redirects=True,
    )
    assert response.status_code == 200
    assert "Logged in user can&#39;t be changed." in response.get_data(as_text=True)
