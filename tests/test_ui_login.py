from cases_backend import db
from cases_backend.auth import User
from cases_backend.auth.role_model import Permission


def test_index_unauthorized(client):
    """
    Check if the default page redirects to the login page
    """
    response = client.get("/admin")
    assert response.status_code == 308
    assert (
        'You should be redirected automatically to target URL: <a href="http://localhost/admin/">http://localhost/admin/'
        in response.get_data(as_text=True)
    )

    # Check correct redirect to Login Page
    response = client.get("/admin", follow_redirects=True)
    assert response.status_code == 200
    assert "<title>SARchive - Login</title>" in response.get_data(as_text=True)


def test_admin_login(client):
    response = client.get("/admin/login")
    assert response.status_code == 200
    assert "<title>SARchive - Login</title>" in response.get_data(as_text=True)

    response = client.post(
        "/admin/login",
        data={"username": "admin", "password": "admin"},
        follow_redirects=True,
    )
    assert response.status_code == 200
    assert "<title>SARchive - Admin</title>" in response.get_data(as_text=True)
    assert 'admin<b class="caret">' in response.get_data(as_text=True)


def test_admin_first_login(client):
    user = User(username="tester", password="test")
    user.add_role(Permission.R_ADMIN)
    db.session.add(user)
    db.session.commit()

    response = client.get("/admin/login")
    assert response.status_code == 200
    assert "<title>SARchive - Login</title>" in response.get_data(as_text=True)

    response = client.post(
        "/admin/login",
        data={"username": "tester", "password": "test"},
        follow_redirects=True,
    )
    assert response.status_code == 200
    assert "<title>SARchive - Change Password</title>" in response.get_data(
        as_text=True
    )
