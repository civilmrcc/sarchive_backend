from cases_backend import db
from cases_backend.auth import User
from cases_backend.auth.role_model import Permission
from .test_data.generate_test_data import generate_full_test_data

cases_query = """cases(filter:{}) {
      cases{
        id
        caseNumber
      }
    }"""

login_mutation_postfix = """
      {
        token
        user {
          id
          username
          isActive
          isPasswordInitial
          roles {
            name
            description
          }
        }
      }"""


def test_admin(run_gql):
    result = run_gql(
        mutation='login(username: "admin", password: "admin")' + login_mutation_postfix
    )
    assert "login" in result["data"]
    assert "token" in result["data"]["login"]
    assert "user" in result["data"]["login"]

    user = result["data"]["login"]["user"]
    assert user["id"] is not None
    assert user["username"] == "admin"
    assert user["isPasswordInitial"] is False  # changed during test setup
    assert user["isActive"] is True

    assert user["roles"][0]["name"] == "Admin"


def test_wrong_password(run_gql, get_error_code):
    result = run_gql(
        mutation='login(username: "admin", password: "xxx")' + login_mutation_postfix
    )
    assert get_error_code(result["errors"][0]["message"]) == "AUTH_NOT_AUTHENTICATED"


def test_user_not_found(run_gql, get_error_code):
    result = run_gql(
        mutation='login(username: "xxx", password: "xxx")' + login_mutation_postfix
    )
    assert get_error_code(result["errors"][0]["message"]) == "AUTH_USER_NOT_FOUND"


def test_user_not_active(run_gql, get_error_code):
    user = User.query.filter_by(username="admin").first()
    user.active = False
    db.session.commit()

    result = run_gql(
        mutation='login(username: "admin", password: "admin")' + login_mutation_postfix
    )
    assert get_error_code(result["errors"][0]["message"]) == "AUTH_USER_NOT_ACTIVE"


def test_change_password(run_gql, login, get_error_code):
    user = User(username="tester", password="test", active=True)
    db.session.add(user)
    db.session.commit()

    token = login("tester", "test")

    # Test different passwords required
    result = run_gql(
        mutation='changePassword(oldPassword: "test", newPassword:"test")',
        token=token,
    )
    assert (
        get_error_code(result["errors"][0]["message"])
        == "USER_PWD_CANT_BE_SAME_AS_CURRENT"
    )

    # Test wrong login
    result = run_gql(
        mutation='changePassword(oldPassword: "testold", newPassword:"test")',
        token=token,
    )
    assert get_error_code(result["errors"][0]["message"]) == "AUTH_WRONG_PASSWORD"

    # Test change
    result = run_gql(
        mutation='changePassword(oldPassword: "test", newPassword:"testnew")',
        token=token,
    )
    assert result["data"]["changePassword"] is True

    # Test login
    token = login("tester", "testnew")
    assert token is not None


def test_password_change_needed(run_gql, login, get_error_code):
    generate_full_test_data()
    user = User(username="tester", password="test", active=True)
    user.add_role(Permission.R_CASE_RESEARCHER)
    db.session.add(user)
    db.session.commit()

    token = login("tester", "test")
    result = run_gql(query=cases_query, token=token)
    assert (
        get_error_code(result["errors"][0]["message"]) == "AUTH_PASSWORD_NEEDS_CHANGE"
    )

    # Change password an re-login
    run_gql(
        mutation='changePassword(oldPassword: "test", newPassword:"testnew")',
        token=token,
    )
    token = login("tester", "testnew")

    result = run_gql(query=cases_query, token=token)
    assert len(result["data"]["cases"]["cases"])


def test_cases_without_login(run_gql, get_error_code):
    result = run_gql(query=cases_query)
    assert get_error_code(result["errors"][0]["message"]) == "AUTH_TOKEN_MISSING"


def test_cases_with_login(run_gql, login):
    generate_full_test_data()
    token = login("admin", "admin")
    result = run_gql(query=cases_query, token=token)
    assert len(result["data"]["cases"]["cases"]) == 3
