import pytest

from cases_backend.auth.role_model import Role, Permission


def test_permission_check():
    with pytest.raises(AssertionError):
        # noinspection PyTypeChecker
        Role.check_permission_parameter([])
        Role.check_permission_parameter({})
        Role.check_permission_parameter({"value": "string"})

    Role.check_permission_parameter({"value": 4})


def test_default_permission():
    role = Role()

    assert role.permissions == 0


def test_has_permission():
    role = Role()
    role.permissions = 2  # hard code to CASE_CREATE to avoid add_permission dependency

    assert role.has_permission(Permission.P_CASE_CREATE) is True
    assert role.has_permission(Permission.P_CASE_UPDATE) is False


def test_add_permission():
    role = Role()
    role.add_permission(Permission.P_CASE_CREATE)

    assert role.permissions == Permission.P_CASE_CREATE["value"]


def test_add_permission_twice():
    role = Role()
    role.add_permission(Permission.P_CASE_CREATE)
    role.add_permission(Permission.P_CASE_CREATE)

    assert role.permissions == Permission.P_CASE_CREATE["value"]


def test_remove_permission():
    role = Role()
    role.add_permission(Permission.P_CASE_UPDATE)
    role.add_permission(Permission.P_CASE_CREATE)
    role.remove_permission(Permission.P_CASE_UPDATE)

    assert role.permissions == Permission.P_CASE_CREATE["value"]


def test_remove_permission_not_existing():
    role = Role()
    role.remove_permission(Permission.P_CASE_UPDATE)

    assert role.permissions == 0


def test_reset_permissions():
    role = Role()
    role.add_permission(Permission.P_CASE_CREATE)

    assert role.permissions == Permission.P_CASE_CREATE["value"]

    role.reset_permissions()

    assert role.permissions == 0


def test_default_roles(client):
    roles = Role.query.all()

    assert len(roles) == 4

    # Check values
    for role in roles:
        if role.name == "CaseResearcher":
            assert role.permissions == 1
            assert role.default is True
        elif role.name == "CaseManager":
            assert role.permissions == 135
            assert role.default is False
        elif role.name == "CategoryManager":
            assert role.permissions == 56
            assert role.default is False
        elif role.name == "Admin":
            assert role.permissions == 511
            assert role.default is False
        else:
            assert role.name == "unknown role name"


def test_rerun_default_roles(client):
    # This should not lead to any errors
    Role.insert_default_roles()

    test_default_roles(client)
