from cases_backend import db
from cases_backend.auth import User
from cases_backend.auth.role_model import Permission
from tests.test_data.generate_test_data import generate_full_test_data

cases_query = """cases(filter:{}) {
      cases{
        id
        caseNumber
      }
    }"""


def test_permission_success(client, run_gql, login):
    generate_full_test_data()

    # Create testuser with the correct permission
    user = User(username="tester", password="test")
    user.active = True
    user.is_password_initial = False
    user.add_role(Permission.R_CASE_RESEARCHER)
    db.session.add(user)
    db.session.commit()

    token = login("tester", "test")
    result = run_gql(query=cases_query, token=token)
    assert len(result["data"]["cases"]["cases"]) == 3


def test_permission_fail(client, login, run_gql, get_error_code):
    generate_full_test_data()

    # Create a testuser w/o the required permission
    user = User(username="tester", password="test")
    user.active = True
    user.is_password_initial = False
    user.remove_role(Permission.R_CASE_RESEARCHER)
    db.session.add(user)
    db.session.commit()

    token = login("tester", "test")
    result = run_gql(query=cases_query, token=token)

    assert "errors" in result
    assert get_error_code(result["errors"][0]["message"]) == "AUTH_MISSING_PERMISSION"
