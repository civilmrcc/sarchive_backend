from cases_backend import db
from cases_backend.auth import User


def test_call_page_without_username(client, login_ui):
    login_ui("admin", "admin")

    response = client.get("/admin/edituser", follow_redirects=True,)

    assert response.status_code == 200
    assert "<title>SARchive - Manage Users</title>" in response.get_data(as_text=True)
    assert "No username was provided." in response.get_data(as_text=True)


def test_call_page_with_invalid_username(client, login_ui):
    login_ui("admin", "admin")

    response = client.post(
        "/admin/edituser", data={"username": "tester"}, follow_redirects=True,
    )

    assert response.status_code == 200
    assert "<title>SARchive - Manage Users</title>" in response.get_data(as_text=True)
    assert "User &#39;tester&#39; not found." in response.get_data(as_text=True)


def test_call_page_with_username(client, login_ui):
    user = User(username="tester", password="test")
    db.session.add(user)
    db.session.commit()

    login_ui("admin", "admin")

    response = client.post(
        "/admin/edituser", data={"username": "tester"}, follow_redirects=True,
    )

    assert response.status_code == 200
    assert "<title>SARchive - Edit User</title>" in response.get_data(as_text=True)


def test_remove_all_attributes(client, login_ui):
    user = User(username="tester", password="test")
    db.session.add(user)
    db.session.commit()

    login_ui("admin", "admin")

    response = client.post(
        "/admin/edituser",
        data={"username": "tester", "submit": "Change+User"},
        follow_redirects=True,
    )

    assert response.status_code == 200
    assert "<title>SARchive - Edit User</title>" in response.get_data(as_text=True)
    assert "User successfully changed." in response.get_data(as_text=True)

    db_user = User.query.filter_by(username="tester").first()

    assert db_user.active is False
    assert db_user.is_password_initial is True
    assert len(db_user.roles) == 0


def test_add_all_attributes(client, login_ui):
    user = User(username="tester", password="test")
    db.session.add(user)
    db.session.commit()

    login_ui("admin", "admin")

    response = client.post(
        "/admin/edituser",
        data={
            "username": "tester",
            "active": "y",
            "CaseResearcher": "y",
            "CaseManager": "y",
            "CategoryManager": "y",
            "Admin": "y",
            "submit": "Change+User",
        },
        follow_redirects=True,
    )

    assert response.status_code == 200
    assert "<title>SARchive - Edit User</title>" in response.get_data(as_text=True)
    assert "User successfully changed." in response.get_data(as_text=True)

    db_user = User.query.filter_by(username="tester").first()

    assert db_user.active is True
    assert db_user.is_password_initial is True
    assert len(db_user.roles) == 4


# def test_create_user_get(client, login_ui):
#     login_ui("admin", "admin")
#
#     response = client.get("/admin/createuser")
#     assert response.status_code == 200
#     assert "<title>SARchive - Create User</title>" in response.get_data(as_text=True)
#
#     # Check default role
#     assert 'checked id="CaseResearcher"' in response.get_data(as_text=True)
#
#
# def test_create_existing_user(client, login_ui):
#     login_ui("admin", "admin")
#
#     response = client.post(
#         "/admin/createuser",
#         data={"username": "admin", "password": "admin", "active": "True"},
#         follow_redirects=True,
#     )
#     assert response.status_code == 200
#     assert "<title>SARchive - Create User</title>" in response.get_data(as_text=True)
#     assert "User already exists." in response.get_data(as_text=True)
#
#
# def test_create_active_user_without_roles(client, login_ui):
#     login_ui("admin", "admin")
#
#     response = client.post(
#         "/admin/createuser",
#         data={"username": "tester", "password": "tester", "active": "True"},
#         follow_redirects=True,
#     )
#     assert response.status_code == 200
#     assert "<title>SARchive - Create User</title>" in response.get_data(as_text=True)
#     assert "User successfully created." in response.get_data(as_text=True)
#
#     user = User.query.filter_by(username="tester").first()
#     assert user.is_password_initial
#     assert user.active
#     assert len(user.roles) == 0
#
#
# def test_create_inactive_user_with_all_roles(client, login_ui):
#     login_ui("admin", "admin")
#
#     response = client.post(
#         "/admin/createuser",
#         data={
#             "username": "tester",
#             "password": "tester",
#             "Admin": "True",
#             "CategoryManager": "True",
#             "CaseManager": "True",
#             "CaseResearcher": "True",
#         },
#         follow_redirects=True,
#     )
#     assert response.status_code == 200
#     assert "<title>SARchive - Create User</title>" in response.get_data(as_text=True)
#     assert "User successfully created." in response.get_data(as_text=True)
#
#     user = User.query.filter_by(username="tester").first()
#     assert user.is_password_initial
#     assert not user.active
#     assert len(user.roles) == 4
