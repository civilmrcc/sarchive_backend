import os.path

import pytest

from cases_backend import create_app, db
from cases_backend.auth.role_model import Role
from cases_backend.auth import User

from migrations.versions.c419894589e8_materialized_view_for_search import (
    view_string,
    index_string,
    index_unique,
    index_occurred,
    index_boat_type,
)

app = create_app("testing")
app_context = app.app_context()
app_context.push()


# Default scope for a fixture is "per function". That means,
# the database is rebuild for each single test. That slows
# down the tests "a bit", but it's the safest way. With better
# understanding on how pytest_pqsql works, this can probably
# be improved. But for now, it just works.
@pytest.fixture
def client(postgresql_db):
    postgresql_db.install_extension("postgis")

    # This happens before the app opens the connection pool,
    # which allows us to overwrite SQLALCHEMY_DATABASE_URI to use our
    # temporary test database.
    os.environ["SQLALCHEMY_DATABASE_URI"] = postgresql_db.postgresql_url
    app.config["SQLALCHEMY_DATABASE_URI"] = postgresql_db.postgresql_url

    # Our fresh test database is empty, so we have to create
    # our full database structure.
    db.create_all()

    Role.insert_default_roles()

    # Create default admin user and change the password, otherwise the
    # 'change password at first login' policy will fail all subsequent
    # tests. The logic itself is tested with the user tests
    User.insert_default_user()
    admin = User.query.filter_by(username="admin").first()
    admin.password = "admin"

    db.session.commit()

    # Manually create the materialized vie for the search according to
    # the DB migration `c419894589e8`. The view is not created based on
    # a model definition but on a raw SQL statement, which is not covered
    # by `db.create_all()`
    postgresql_db.engine.execute(view_string)
    postgresql_db.engine.execute(index_string)
    postgresql_db.engine.execute(index_unique)
    postgresql_db.engine.execute(index_occurred)
    postgresql_db.engine.execute(index_boat_type)

    yield app.test_client()

    # In a session scoped fixture we would have to cleanup
    # the database here.
    db.session.close()


def exec_gql(client, gql, token=None):
    headers = {"Content-Type": "application/json"}
    if token:
        headers["Authorization"] = "Bearer " + token
    result = client.post("/", headers=headers, json={"query": gql}).json
    return result


@pytest.fixture
def login(client):
    def get_token(username, password):
        return exec_gql(
            client,
            """
            mutation {{
                login(username: "{}", password: "{}") {{ token }}
            }}
            """.format(
                username, password
            ),
        )["data"]["login"]["token"]

    yield get_token


@pytest.fixture
def login_ui(client):
    def decorated_login_ui(username, password):
        client.post(
            "/admin/login",
            data={"username": username, "password": password},
            follow_redirects=True,
        )

    yield decorated_login_ui


@pytest.fixture
def run_gql(client):
    def fkt(query=None, mutation=None, token=None):
        template = """{}{{
            {}
        }}"""
        if mutation:
            op = "mutation", mutation
        if query:
            op = "query", query
        result = exec_gql(client, template.format(*op), token)
        return result

    return fkt


@pytest.fixture
def get_error_code():
    def fkt(message: str):
        return message.split(sep=":", maxsplit=1)[0]

    return fkt
