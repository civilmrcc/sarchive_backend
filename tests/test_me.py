me_query = """
  me {
    id
    username
    isActive
    isPasswordInitial
    roles {
      name
      description
    }
  }"""


def test_me(run_gql, login):
    token = login("admin", "admin")
    result = run_gql(query=me_query, token=token)

    user = result["data"]["me"]
    assert user["id"] is not None
    assert user["username"] == "admin"
    assert user["isPasswordInitial"] is False  # changed during test setup
    assert user["isActive"] is True

    assert user["roles"][0]["name"] == "Admin"


def test_me_without_login(run_gql, get_error_code):
    result = run_gql(query=me_query)
    assert get_error_code(result["errors"][0]["message"]) == "AUTH_TOKEN_MISSING"
