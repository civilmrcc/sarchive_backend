settings_query = """
    appSettings{
        errorCodes
    }
"""


def test_settings_return_object(run_gql):
    result = run_gql(query=settings_query)["data"]
    assert len(result["appSettings"]) == 1
    assert "errorCodes" in result["appSettings"][0]
