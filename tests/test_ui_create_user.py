from cases_backend.auth import User


def test_create_user_get(client, login_ui):
    login_ui("admin", "admin")

    response = client.get("/admin/createuser")
    assert response.status_code == 200
    assert "<title>SARchive - Create User</title>" in response.get_data(as_text=True)

    # Check default role
    assert 'checked id="CaseResearcher"' in response.get_data(as_text=True)


def test_create_existing_user(client, login_ui):
    login_ui("admin", "admin")

    response = client.post(
        "/admin/createuser",
        data={"username": "admin", "password": "admin", "active": "True"},
        follow_redirects=True,
    )
    assert response.status_code == 200
    assert "<title>SARchive - Create User</title>" in response.get_data(as_text=True)
    assert "User already exists." in response.get_data(as_text=True)


def test_create_active_user_without_roles(client, login_ui):
    login_ui("admin", "admin")

    response = client.post(
        "/admin/createuser",
        data={"username": "tester", "password": "tester", "active": "True"},
        follow_redirects=True,
    )
    assert response.status_code == 200
    assert "<title>SARchive - Create User</title>" in response.get_data(as_text=True)
    assert "User successfully created." in response.get_data(as_text=True)

    user = User.query.filter_by(username="tester").first()
    assert user.is_password_initial
    assert user.active
    assert len(user.roles) == 0


def test_create_inactive_user_with_all_roles(client, login_ui):
    login_ui("admin", "admin")

    response = client.post(
        "/admin/createuser",
        data={
            "username": "tester",
            "password": "tester",
            "Admin": "True",
            "CategoryManager": "True",
            "CaseManager": "True",
            "CaseResearcher": "True",
        },
        follow_redirects=True,
    )
    assert response.status_code == 200
    assert "<title>SARchive - Create User</title>" in response.get_data(as_text=True)
    assert "User successfully created." in response.get_data(as_text=True)

    user = User.query.filter_by(username="tester").first()
    assert user.is_password_initial
    assert not user.active
    assert len(user.roles) == 4
