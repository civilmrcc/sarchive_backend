from .test_data.generate_test_data import generate_full_test_data

case_query = """
  case(id: "<id>") {
    id
    positions {
      id
      caseId
      timestamp
      longitude
      latitude
      speedOverGround
      courseOverGround
    }
  }
"""


def test_delete_position_from_case(run_gql, login, postgresql_db):
    case_obj = generate_full_test_data()[0]
    token = login("admin", "admin")

    query = case_query.replace("<id>", case_obj.id)
    result = run_gql(query=query, token=token)
    case = result["data"]["case"]

    assert len(case["positions"]) == 4

    result = run_gql(
        mutation="""
            deletePositionFromCase(id: "4", caseId:"%s")
            """
        % case_obj.id,
        token=token,
    )

    assert result["data"]["deletePositionFromCase"] is True

    # Deleting the same twice should not work
    result = run_gql(
        mutation="""
            deletePositionFromCase(id: "4", caseId:"%s")
            """
        % case_obj.id,
        token=token,
    )["data"]

    assert result["deletePositionFromCase"] is False

    # Reload the case
    result = run_gql(query=query, token=token)
    case = result["data"]["case"]

    assert len(case["positions"]) == 3

    position_ids = [position["id"] for position in case["positions"]]
    assert position_ids == ["1", "2", "3"]


def test_add_position_to_case(run_gql, login, postgresql_db):
    case_obj = generate_full_test_data()[0]
    token = login("admin", "admin")

    query = case_query.replace("<id>", case_obj.id)
    result = run_gql(query=query, token=token)["data"]
    case = result["case"]

    assert len(case["positions"]) == 4

    result = run_gql(
        mutation="""
          addPositionToCase(
            caseId: "%s"
            position: {
                latitude: -99
                longitude: 88
                timestamp: "2020-12-31 11:22:33"
                speedOverGround: 3.14159
                courseOverGround: 9.247
            }
          ) {
            id
          }"""
        % case_obj.id,
        token=token,
    )

    assert result["data"]["addPositionToCase"]["id"] == "5"

    # Reload the case
    result = run_gql(query=query, token=token)
    case = result["data"]["case"]

    assert len(case["positions"]) == 5


def test_add_position_to_case_with_init_values(run_gql, login, postgresql_db):
    case_obj = generate_full_test_data()[0]
    token = login("admin", "admin")

    query = case_query.replace("<id>", case_obj.id)
    result = run_gql(query=query, token=token)["data"]
    case = result["case"]

    assert len(case["positions"]) == 4

    result = run_gql(
        mutation="""
          addPositionToCase(
            caseId: "%s"
            position: {
                latitude: -99
                longitude: 88
                timestamp: "2020-12-31 11:22:33"
            }
          ) {
            id
          }"""
        % case_obj.id,
        token=token,
    )["data"]

    assert result["addPositionToCase"]["id"] == "5"

    # Reload the case
    result = run_gql(query=query, token=token)["data"]
    case = result["case"]

    assert len(case["positions"]) == 5

    position_ids = [position["id"] for position in case["positions"]]
    assert position_ids == ["1", "2", "3", "4", "5"]

    position = case["positions"][4]
    assert position == {
        "caseId": case_obj.id,
        "id": "5",
        "latitude": -99,
        "longitude": 88,
        "timestamp": "2020-12-31 11:22:33",
        "speedOverGround": None,
        "courseOverGround": None,
    }


def test_return_data(run_gql, login, postgresql_db):
    token = login("admin", "admin")

    result = run_gql(
        mutation="""
        createCase(case: {
            caseNumber: "test_case"
            position: {
                latitude: -88
                longitude: 77
                timestamp: "2020-12-01 11:22:33"
                speedOverGround: 1
                courseOverGround: 2
            }
        }) {
           id
           caseNumber
           positions {
              id
              caseId
              timestamp
              longitude
              latitude
              speedOverGround
              courseOverGround
            }
        }""",
        token=token,
    )["data"]

    case = result["createCase"]

    # Check the return value from create
    assert len(case["positions"]) == 1
    assert case["positions"][0] == {
        "id": "0",
        "caseId": case["id"],
        "latitude": -88.0,
        "longitude": 77.0,
        "timestamp": "2020-12-01 11:22:33",
        "speedOverGround": 1.0,
        "courseOverGround": 2.0,
    }

    # Add Position to case
    result = run_gql(
        mutation=f"""
          addPositionToCase(
            caseId: "{case["id"]}"
            position: {{
                latitude: -99
                longitude: 88
                timestamp: "2020-12-31 11:22:33"
                speedOverGround: 3.14159
                courseOverGround: 9.247
            }}
          ) {{
              id
              caseId
              timestamp
              longitude
              latitude
              speedOverGround
              courseOverGround
          }}""",
        token=token,
    )

    # Check the return value from add position
    assert result["data"]["addPositionToCase"] == {
        "id": "1",
        "caseId": case["id"],
        "latitude": -99.0,
        "longitude": 88.0,
        "timestamp": "2020-12-31 11:22:33",
        "speedOverGround": 3.14159,
        "courseOverGround": 9.247,
    }

    # Read Case and check positions
    query = case_query.replace("<id>", case["id"])
    result = run_gql(query=query, token=token)
    case = result["data"]["case"]

    assert len(case["positions"]) == 2

    assert case["positions"][0] == {
        "id": "0",
        "caseId": case["id"],
        "latitude": -88.0,
        "longitude": 77.0,
        "timestamp": "2020-12-01 11:22:33",
        "speedOverGround": 1.0,
        "courseOverGround": 2.0,
    }

    assert case["positions"][1] == {
        "id": "1",
        "caseId": case["id"],
        "latitude": -99.0,
        "longitude": 88.0,
        "timestamp": "2020-12-31 11:22:33",
        "speedOverGround": 3.14159,
        "courseOverGround": 9.247,
    }
