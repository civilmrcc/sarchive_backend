from .test_data.generate_test_data import generate_full_test_data

case_query = """
  case(id: "<id>") {
    id
    caseNumber
    positions {
      id
      caseId
      timestamp
      longitude
      latitude
    }
    peopleOnBoard {
      women
      men
      minors
      total
      medical
      missing
      drowned
    }
    boat {
      type
      color
      engineStatus
    }
    links
    freetext
    tags {
      id
      name
    }
    timestamp
  }
"""


def test_case_complete(run_gql, login, postgresql_db):
    case_1, case_2, case_3 = generate_full_test_data()
    token = login("admin", "admin")

    query = case_query.replace("<id>", case_1.id)
    result = run_gql(query=query, token=token)

    case = result["data"]["case"]

    # General Case Data
    assert case["id"] == case_1.id
    assert case["caseNumber"] == "case_1"
    assert case["freetext"] == "Test Case 1"
    assert case["timestamp"] == "2020-01-01 11:17:55"

    assert case["boat"] == {
        "type": "RUBBER",
        "color": "RED",
        "engineStatus": "Running",
    }


def test_people_on_board(run_gql, login, postgresql_db):
    case_1, case_2, case_3 = generate_full_test_data()
    token = login("admin", "admin")

    query = case_query.replace("<id>", case_1.id)
    result = run_gql(query=query, token=token)

    case = result["data"]["case"]
    people_on_board = case["peopleOnBoard"]
    assert people_on_board == {
        "drowned": 7,
        "medical": 5,
        "men": 3,
        "minors": 4,
        "missing": 6,
        "total": 27,
        "women": 2,
    }

    pob_sum = (
        people_on_board["drowned"]
        + people_on_board["medical"]
        + people_on_board["men"]
        + people_on_board["minors"]
        + people_on_board["missing"]
        + people_on_board["women"]
    )
    assert pob_sum == 27


def test_positions(run_gql, login, postgresql_db):
    case_1, case_2, case_3 = generate_full_test_data()
    token = login("admin", "admin")

    query = case_query.replace("<id>", case_1.id)
    result = run_gql(query=query, token=token)

    case = result["data"]["case"]
    assert len(case["positions"]) == 4
    assert case["positions"][0] == {
        "caseId": case_1.id,
        "id": "1",
        "latitude": 10,
        "longitude": -10,
        "timestamp": "2020-01-02 11:17:55",
    }


def test_case_with_tags(run_gql, login, postgresql_db):
    case_1, case_2, case_3 = generate_full_test_data()
    token = login("admin", "admin")

    query = case_query.replace("<id>", case_1.id)
    result = run_gql(query=query, token=token)

    # Case 1 with 3 tags
    case = result["data"]["case"]
    assert len(case["tags"]) == 2

    assert case["tags"][0]["name"] == "other"
    assert case["tags"][1]["name"] == "test tag"
