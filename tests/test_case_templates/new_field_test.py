# from cases_backend.models import Case
#
# gql_field_name = "field name in <graph QL interface>"
# db_field_name = "field name in DB moodel"
# values_to_be_tested = ["<value1>", "<value2>"]
# values_for_update = ["<value1>", "<value2>"]  # Always provide tuple
# value_for_exception_test = "<invalid value>"  # Invalid value or None
#
#
# def test_create_and_read_case(postgresql_db, run_gql, login):
#     token = login("admin", "admin")
#
#     for value_to_be_tested in values_to_be_tested:
#         result = run_gql(
#             mutation=f"""
#             createCase(case: {{
#                 caseNumber: "test_case"
#                 {gql_field_name}: {value_to_be_tested}
#             }}) {{
#                id
#                {gql_field_name}
#             }}""",
#             token=token,
#         )
#
#         case = result["data"]["createCase"]
#
#         # Check if the correct value is returned
#         assert case[gql_field_name] == value_to_be_tested
#
#         # Check if the value is written to the database
#         case_obj = Case.query.filter_by(id=case["id"]).first()
#         assert getattr(case_obj, db_field_name) == value_to_be_tested
#
#
# def test_update_case(postgresql_db, run_gql, login):
#     token = login("admin", "admin")
#
#     # Create Case
#     result = run_gql(
#         mutation=f"""
#         createCase(case: {{
#             caseNumber: "test_case"
#             {gql_field_name}: {values_for_update[0]}
#         }}) {{
#            id
#            {gql_field_name}
#         }}""",
#         token=token,
#     )
#
#     case = result["data"]["createCase"]
#
#     # Check if the correct value is returned
#     assert case[gql_field_name] == values_for_update[0]
#
#     # Check if the value is written to the database
#     case_obj = Case.query.filter_by(id=case["id"]).first()
#     assert getattr(case_obj, db_field_name) == values_for_update[0]
#
#     # Update Case
#     result = run_gql(
#         mutation=f"""
#         updateCase(case: {{
#             id: "{case["id"]}",
#             {gql_field_name}: {values_for_update[1]}
#         }}) {{
#             id
#             {gql_field_name}
#         }}""",
#         token=token,
#     )
#
#     # Check the object
#     updated_case = result["data"]["updateCase"]
#     assert updated_case["id"] == case["id"]
#     assert updated_case[gql_field_name] == values_for_update[1]
#
#
# def test_invalid_value(postgresql_db, run_gql, login):
#     # If the field does not have invalid input, return
#     if value_for_exception_test is None:
#         return
#
#     token = login("admin", "admin")
#
#     result = run_gql(
#         mutation=f"""
#         createCase(case: {{
#             caseNumber: "test_case"
#             {gql_field_name}: {value_for_exception_test}
#         }}) {{
#            id
#            {gql_field_name}
#         }}""",
#         token=token,
#     )
#
#     assert result["errors"]
