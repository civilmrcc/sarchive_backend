import pytest

from cases_backend.auth.decorators import requires_authentication, requires_permission
from cases_backend.auth.role_model import Permission


def test_requires_auth():
    @requires_authentication
    def test_method():
        pass

    method = test_method
    assert getattr(method, "_requires_authentication", None) is not None
    assert getattr(method, "_requires_authentication", None) is True


def test_requires_permissions_wrong_parameter():
    with pytest.raises(AssertionError):

        @requires_permission(permission="wrong parameter")
        def test_method():
            pass


def test_requires_permissions():
    @requires_permission(permission=Permission.P_CASE_CREATE)
    def test_method():
        pass

    method = test_method
    assert getattr(method, "_requires_permission", None) is not None
    assert getattr(method, "_requires_permission", None)["value"] == 2
