from datetime import datetime, timedelta

import jwt
import pytest
from flask import current_app

from cases_backend import db
from cases_backend.auth import User
from cases_backend.auth.role_model import Permission

# Hint: All user test cases need the client fixture, because the user has a
#       relation to the role object which is loaded from the DB


def test_password_setter(client):
    user = User(
        username="tester", password="test", active=True, is_password_initial=True
    )
    assert user.password_hash is not None


def test_no_password_getter(client):
    user = User(
        username="tester", password="test", active=True, is_password_initial=True
    )
    with pytest.raises(AttributeError):
        user.password()


def test_password_verification(client):
    user = User(
        username="tester", password="test", active=True, is_password_initial=True
    )
    assert user.verify_password("test") is True
    assert user.verify_password("wrongtest") is False


def test_password_salts_are_random(client):
    user1 = User(
        username="tester1", password="test", active=True, is_password_initial=True
    )
    user2 = User(
        username="tester2", password="test", active=True, is_password_initial=True
    )
    assert user1.password_hash != user2.password_hash


def test_token_creation(client):
    user = User.query.filter_by(username="admin").first()

    assert user.create_token() is not None


def test_token_validation_no_sub(client, get_error_code):
    app_secret = current_app.config["APP_SECRET"]
    payload = {
        "exp": datetime.utcnow() + timedelta(days=0, seconds=3600),
        "iat": datetime.utcnow(),
    }

    token = jwt.encode(payload, app_secret, algorithm="HS256").decode("utf-8")

    with pytest.raises(RuntimeError) as excinfo:
        User.get_user_by_token(token)
    assert get_error_code(str(excinfo.value)) == "AUTH_TOKEN_INVALID"


def test_token_validation_wrong_sub(client, get_error_code):
    app_secret = current_app.config["APP_SECRET"]
    payload = {
        "exp": datetime.utcnow() + timedelta(days=0, seconds=3600),
        "iat": datetime.utcnow(),
        "sub": "tester",
    }

    token = jwt.encode(payload, app_secret, algorithm="HS256").decode("utf-8")

    with pytest.raises(RuntimeError) as excinfo:
        User.get_user_by_token(token)
    assert get_error_code(str(excinfo.value)) == "AUTH_USER_NOT_FOUND"


def test_token_validation_user_inactive(client, get_error_code):
    user = User.query.filter_by(username="admin").first()
    user.active = False
    db.session.commit()

    app_secret = current_app.config["APP_SECRET"]
    payload = {
        "exp": datetime.utcnow() + timedelta(days=0, seconds=3600),
        "iat": datetime.utcnow(),
        "sub": "admin",
    }

    token = jwt.encode(payload, app_secret, algorithm="HS256").decode("utf-8")

    with pytest.raises(RuntimeError) as excinfo:
        User.get_user_by_token(token)
    assert get_error_code(str(excinfo.value)) == "AUTH_USER_NOT_ACTIVE"


def test_user_defaults(client):
    user = User(username="tester", password="test")
    user.add_role(Permission.R_CASE_RESEARCHER)
    db.session.add(user)
    db.session.commit()

    db_user = User.query.filter_by(username="tester").first()

    assert db_user.active is True
    assert db_user.is_active == db_user.active

    assert db_user.get_id() == db_user.id

    assert db_user.is_password_initial is True

    assert len(db_user.roles) == 1
    assert db_user.roles[0].name == Permission.R_CASE_RESEARCHER


def test_add_role_to_user(client):
    user = User(username="tester", password="test")
    db.session.add(user)
    db.session.commit()

    # Check the role list, that it's not in yet
    assert not any(role.name == Permission.R_CATEGORY_MANAGER for role in user.roles)

    assert user.add_role(Permission.R_CATEGORY_MANAGER) is True

    # Check the role list, if the role is added
    assert any(role.name == Permission.R_CATEGORY_MANAGER for role in user.roles)

    # Check adding a role again
    assert user.add_role(Permission.R_CATEGORY_MANAGER) is False

    # Check adding a non existing role
    assert user.add_role("DoesNotExist") is False


def test_remove_role_from_user(client):
    user = User(username="tester", password="test")
    user.add_role(Permission.R_CASE_RESEARCHER)
    db.session.add(user)
    db.session.commit()

    assert user.remove_role(Permission.R_CASE_RESEARCHER) is True

    # Second time should return False, since role is not assigned anymore
    assert user.remove_role(Permission.R_CASE_RESEARCHER) is False


def test_remove_all_roles_from_user(client):
    user = User(username="tester", password="test")
    user.add_role(Permission.R_CASE_RESEARCHER)
    user.add_role(Permission.R_CASE_MANAGER)
    user.add_role(Permission.R_CATEGORY_MANAGER)
    user.add_role(Permission.R_ADMIN)
    db.session.add(user)
    db.session.commit()

    assert len(user.roles) == 4
    user.remove_all_roles()
    db.session.commit()
    assert len(user.roles) == 0

    # Check if the user still exists or if there is sth. wrong with the
    # delete cascade
    user = User.query.filter_by(username="tester").first()
    assert user is not None


def test_has_permission(client):
    user = User(username="tester", password="test")
    user.add_role(Permission.R_CASE_RESEARCHER)
    db.session.add(user)
    db.session.commit()

    assert user.has_permission(Permission.P_CASE_READ) is True
    assert user.has_permission(Permission.P_USER_MANAGE) is False

    user.add_role(Permission.R_ADMIN)
    assert user.has_permission(Permission.P_USER_MANAGE) is True


def test_admin_defaults(client):
    user = User.query.filter_by(username="admin").first()

    assert user.is_password_initial is False  # changed by test config
    assert user.active is True

    assert len(user.roles) == 1
    assert user.roles[0].name == Permission.R_ADMIN
