from cases_backend.models import FullCaseForSearch
from tests.test_data.generate_test_data import generate_full_test_data


def test_total_number_of_records(client, postgresql_db):
    generate_full_test_data()

    postgresql_db.engine.execute("REFRESH MATERIALIZED VIEW full_cases_mv;")
    all_entries = FullCaseForSearch.query.all()

    assert len(all_entries) == 4


def test_search_view_update_after_new_case(postgresql_db, run_gql, login):
    generate_full_test_data()
    token = login("admin", "admin")

    run_gql(
        mutation="""
        createCase(case: {
            caseNumber: "Search Test Case"
        }) {
           id
           caseNumber
        }""",
        token=token,
    )["data"]

    all_entries = FullCaseForSearch.query.all()
    assert len(all_entries) == 5


def test_search_view_update_after_case_update(postgresql_db, run_gql, login):
    case_obj = generate_full_test_data()[0]
    token = login("admin", "admin")

    # Remove all tags from the case and create a new one and see if the view
    # updates
    run_gql(
        mutation="""
            updateCase(
                case: {
                    tags: [
                        {
                            name: "new test tag"
                        }
                    ]
                    id: "%s"
                }
            ) {
                id
            }"""
        % case_obj.id,
        token=token,
    )

    all_entries = FullCaseForSearch.query.all()
    assert len(all_entries) == 3

    # Only one should have the new tag
    one_entry = FullCaseForSearch.query.filter_by(tag="new test tag").all()
    assert len(one_entry) == 1


def test_text_filter(client, postgresql_db, login, run_gql):
    c1, c2, c3 = generate_full_test_data()
    postgresql_db.engine.execute("REFRESH MATERIALIZED VIEW full_cases_mv;")

    token = login("admin", "admin")
    case_query = """
      cases(
        filter: {
          text: "minimal"
        }
      ) {
        cases{id}
      }
    """

    result = run_gql(query=case_query, token=token)

    assert len(result["data"]["cases"]["cases"]) == 1
    assert result["data"]["cases"]["cases"][0]["id"] == c3.id


def test_tag_filter(client, postgresql_db, login, run_gql):
    c1, c2, c3 = generate_full_test_data()
    postgresql_db.engine.execute("REFRESH MATERIALIZED VIEW full_cases_mv;")

    token = login("admin", "admin")
    case_query = """
      cases(
        filter: {
          tags: [{ name: "another" }]
        }
      ) {
        cases{id}
      }
    """

    result = run_gql(query=case_query, token=token)

    assert len(result["data"]["cases"]["cases"]) == 1
    assert result["data"]["cases"]["cases"][0]["id"] == c2.id


def test_date_from_invalid_format(
    client, postgresql_db, login, run_gql, get_error_code
):
    generate_full_test_data()
    postgresql_db.engine.execute("REFRESH MATERIALIZED VIEW full_cases_mv;")

    token = login("admin", "admin")
    case_query = """
      cases(
        filter: {
            caseDateFrom: "invalid input"
        }
      ) {
        cases{id}
      }
    """

    result = run_gql(query=case_query, token=token)
    assert "errors" in result
    assert get_error_code(result["errors"][0]["message"]) == "INVALID_DATE_FORMAT"


def test_date_to_invalid_format(client, postgresql_db, login, run_gql, get_error_code):
    generate_full_test_data()
    postgresql_db.engine.execute("REFRESH MATERIALIZED VIEW full_cases_mv;")

    token = login("admin", "admin")
    case_query = """
      cases(
        filter: {
            caseDateTo: "invalid input"
        }
      ) {
        cases{id}
      }
    """

    result = run_gql(query=case_query, token=token)
    assert "errors" in result
    assert get_error_code(result["errors"][0]["message"]) == "INVALID_DATE_FORMAT"


def test_date_from(client, postgresql_db, login, run_gql):
    c1, c2, c3 = generate_full_test_data()
    postgresql_db.engine.execute("REFRESH MATERIALIZED VIEW full_cases_mv;")

    token = login("admin", "admin")
    case_query = """
      cases(
        filter: {
            caseDateFrom: "2020-02-20 21:08:00"
        }
      ) {
        cases{id}
      }
    """

    result = run_gql(query=case_query, token=token)

    assert len(result["data"]["cases"]["cases"]) == 1
    assert result["data"]["cases"]["cases"][0]["id"] == c3.id


def test_date_to(client, postgresql_db, login, run_gql):
    c1, c2, c3 = generate_full_test_data()
    postgresql_db.engine.execute("REFRESH MATERIALIZED VIEW full_cases_mv;")

    token = login("admin", "admin")
    case_query = """
      cases(
        filter: {
            caseDateTo: "2020-01-24 00:00:00"
        }
      ) {
        cases{id}
      }
    """

    result = run_gql(query=case_query, token=token)

    assert len(result["data"]["cases"]["cases"]) == 1
    assert result["data"]["cases"]["cases"][0]["id"] == c1.id


def test_date_between(client, postgresql_db, login, run_gql):
    c1, c2, c3 = generate_full_test_data()
    postgresql_db.engine.execute("REFRESH MATERIALIZED VIEW full_cases_mv;")

    token = login("admin", "admin")
    case_query = """
      cases(
        filter: {
            caseDateFrom: "2020-01-20 21:08:00"
            caseDateTo: "2020-02-24 00:00:00"
        }
      ) {
        cases{id}
      }
    """

    result = run_gql(query=case_query, token=token)

    assert len(result["data"]["cases"]["cases"]) == 1
    assert result["data"]["cases"]["cases"][0]["id"] == c2.id


def test_boat_types(client, postgresql_db, login, run_gql):
    c1, c2, c3 = generate_full_test_data()
    postgresql_db.engine.execute("REFRESH MATERIALIZED VIEW full_cases_mv;")

    token = login("admin", "admin")
    case_query = """
      cases(
        filter: {
            boatTypes:[WOOD]
        }
      ) {
        cases{id}
      }
    """

    result = run_gql(query=case_query, token=token)

    assert len(result["data"]["cases"]["cases"]) == 1
    assert result["data"]["cases"]["cases"][0]["id"] == c2.id


def test_boat_colors(client, postgresql_db, login, run_gql):
    c1, c2, c3 = generate_full_test_data()
    postgresql_db.engine.execute("REFRESH MATERIALIZED VIEW full_cases_mv;")

    token = login("admin", "admin")
    case_query = """
      cases(
        filter: {
            boatColors:[RED]
        }
      ) {
        cases{id}
      }
    """

    result = run_gql(query=case_query, token=token)

    assert len(result["data"]["cases"]["cases"]) == 1
    assert result["data"]["cases"]["cases"][0]["id"] == c1.id
