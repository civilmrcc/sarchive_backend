def test_logout(client, login_ui):
    login_ui("admin", "admin")

    response = client.get("/admin/logout", follow_redirects=True)

    assert response.status_code == 200
    assert "<title>SARchive - Login</title>" in response.get_data(as_text=True)
    assert "You have been logged out." in response.get_data(as_text=True)
