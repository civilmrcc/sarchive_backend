from cases_backend.models import Tag, Case

from .test_data.generate_test_data import generate_full_test_data


def test_minimal_case(postgresql_db, run_gql, login):
    generate_full_test_data()
    token = login("admin", "admin")

    result = run_gql(
        mutation="""
        createCase(case: {
            caseNumber: "test_case"
        }) {
           id
           caseNumber
        }""",
        token=token,
    )["data"]

    case = result["createCase"]

    # Does the auto counter in postgresql work?
    assert case["id"] is not None
    assert case["caseNumber"] == "test_case"


def test_full_case(postgresql_db, run_gql, login):
    generate_full_test_data()
    token = login("admin", "admin")

    tags = Tag.query.all()

    result = run_gql(
        mutation="""
            createCase(
                case: {
                boat: { color: RED, engineStatus: "Running", type: RUBBER }
                caseNumber: "case_new"
                tags: [
                    { name: "%s" }
                    { name: "%s" }
                ]
                freetext: "Test Case New"
                links: null
                peopleOnBoard: {
                    drowned: 7
                    medical: 5
                    men: 3
                    minors: 4
                    missing: 6
                    total: 27
                    women: 2
                }
                position: {
                    latitude: 10
                    longitude: -10
                    timestamp: "2020-01-02 11:17:55"
                    speedOverGround: 3.14159
                    courseOverGround: 9.247
                }
                timestamp: "2020-01-01 11:17:55"
                }
            ) {
                id
                caseNumber
                positions {
                    id
                    caseId
                    timestamp
                    longitude
                    latitude
                    speedOverGround
                    courseOverGround
                }
                peopleOnBoard {
                    women
                    men
                    minors
                    total
                    medical
                    missing
                    drowned
                }
                boat {
                    type
                    color
                    engineStatus
                }
                links
                freetext
                tags {
                    id
                    name
                }
                timestamp
                }"""
        % (tags[0].name, tags[1].name),
        token=token,
    )

    case = result["data"]["createCase"]

    # General Case Data
    assert case["id"] is not None
    assert case["caseNumber"] == "case_new"
    assert case["freetext"] == "Test Case New"
    assert case["timestamp"] == "2020-01-01 11:17:55"

    assert case["boat"] == {
        "type": "RUBBER",
        "color": "RED",
        "engineStatus": "Running",
    }

    # Peopple on Board
    people_on_board = case["peopleOnBoard"]
    assert people_on_board == {
        "drowned": 7,
        "medical": 5,
        "men": 3,
        "minors": 4,
        "missing": 6,
        "total": 27,
        "women": 2,
    }

    pob_sum = (
        people_on_board["drowned"]
        + people_on_board["medical"]
        + people_on_board["men"]
        + people_on_board["minors"]
        + people_on_board["missing"]
        + people_on_board["women"]
    )
    assert pob_sum == 27

    # Position
    assert len(case["positions"]) == 1
    assert case["positions"][0] == {
        "caseId": case["id"],
        "id": "0",
        "latitude": 10,
        "longitude": -10,
        "timestamp": "2020-01-02 11:17:55",
        "speedOverGround": 3.14159,
        "courseOverGround": 9.247,
    }

    # Tags
    assert len(case["tags"]) == 2

    assert case["tags"][0]["id"] == tags[0].id  # First Tag
    assert case["tags"][0]["name"] == tags[0].name
    assert case["tags"][1]["id"] == tags[1].id  # First Tag
    assert case["tags"][1]["name"] == tags[1].name


def test_update_case(postgresql_db, run_gql, login):
    case_obj = generate_full_test_data()[0]
    tags = Tag.query.all()
    token = login("admin", "admin")

    # Modify all fields, remove the existing tags, assign an existing one
    # and implicitely create a new tag and assign it.
    result = run_gql(
        mutation="""
            updateCase(
                case: {
                boat: { color: BLACK, engineStatus: "Broken", type: WOOD }
                caseNumber: "case_1_altered"
                tags: [
                    { name: "%s" }
                ]
                freetext: "Test Case 1 altered"
                id: "%s"
                links: ["http://altered.com"]
                peopleOnBoard: {
                    drowned: 1
                    medical: 2
                    men: 3
                    minors: 4
                    missing: 5
                    total: 6
                    women: 7
                }
                timestamp: "2020-11-11 11:17:55"
                }
            ) {
                id
                caseNumber
                positions {
                    id
                    caseId
                    timestamp
                    longitude
                    latitude
                    speedOverGround
                    courseOverGround
                }
                peopleOnBoard {
                    women
                    men
                    minors
                    total
                    medical
                    missing
                    drowned
                }
                boat {
                    type
                    color
                    engineStatus
                }
                links
                freetext
                tags {
                    id
                    name
                }
                timestamp
            }"""
        % (tags[2].name, case_obj.id),
        token=token,
    )

    # Check the object
    case = result["data"]["updateCase"]

    # General Case Data
    assert case["id"] == case_obj.id
    assert case["caseNumber"] == "case_1_altered"
    assert case["freetext"] == "Test Case 1 altered"
    assert case["links"][0] == "http://altered.com"
    assert case["timestamp"] == "2020-11-11 11:17:55"

    assert case["boat"] == {
        "type": "WOOD",
        "color": "BLACK",
        "engineStatus": "Broken",
    }

    # People on Board
    people_on_board = case["peopleOnBoard"]
    assert people_on_board == {
        "drowned": 1,
        "medical": 2,
        "men": 3,
        "minors": 4,
        "missing": 5,
        "total": 6,
        "women": 7,
    }

    # Tags
    assert len(case["tags"]) == 1

    assert case["tags"][0]["name"] == tags[2].name


def test_remove_all_tags_from_case(postgresql_db, run_gql, login):
    case_obj = generate_full_test_data()[0]
    token = login("admin", "admin")

    # Modify all fields, remove the existing tags, assign an existing one
    # and implicitly create a new tag and assign it.
    run_gql(
        mutation="""
            updateCase(
                case: {
                    tags: []
                    id: "%s"
                }
            ) {
                id
            }"""
        % case_obj.id,
        token=token,
    )

    # Check the object
    case = Case.query.filter_by(id=case_obj.id).first()
    # General Case Data
    assert case.id == case_obj.id

    # Tags
    assert case.tags.count() == 0


def test_creation_of_new_tags_from_case(postgresql_db, run_gql, login):
    case_obj = generate_full_test_data()[0]
    token = login("admin", "admin")

    # Modify all fields, remove the existing tags, assign an existing one
    # and implicitly create a new tag and assign it.
    run_gql(
        mutation="""
            updateCase(
                case: {
                    tags: [
                        {
                            name: "new test tag"
                        }
                    ]
                    id: "%s"
                }
            ) {
                id
            }"""
        % case_obj.id,
        token=token,
    )

    # Test if a new tag was created
    tags = Tag.query.all()

    assert len(tags) == 4
    assert tags[3].name == "new test tag"


def test_not_overwriting_not_provided_values(postgresql_db, run_gql, login):
    case_obj = generate_full_test_data()[0]
    token = login("admin", "admin")

    # Modify all fields, remove the existing tags, assign an existing one
    # and implicitly create a new tag and assign it.
    run_gql(
        mutation="""
            updateCase(
                case: {
                    id: "%s"
                }
            ) {
                id
            }"""
        % case_obj.id,
        token=token,
    )

    # Test if a new tag was created
    case = Case.query.filter_by(id=case_obj.id).first()

    assert case.case_number == "case_1"
    assert case.freetext == "Test Case 1"
    assert case.links[0].url == "http://www.test.com"

    assert case.boat_details.boat_type == "RUBBER"
    assert case.boat_details.color == "RED"
    assert case.boat_details.engine_status == "Running"

    # People on Board
    assert case.people_on_board.drowned == 7
    assert case.people_on_board.medical == 5
    assert case.people_on_board.men == 3
    assert case.people_on_board.minors == 4
    assert case.people_on_board.missing == 6
    assert case.people_on_board.total == 27
    assert case.people_on_board.women == 2

    # Tags
    assert case.tags.count() == 2
