def test_change_password(client, login_ui):
    login_ui("admin", "admin")

    response = client.get("/admin/changepassword")
    assert response.status_code == 200
    assert "<title>SARchive - Change Password</title>" in response.get_data(
        as_text=True
    )

    response = client.post(
        "/admin/changepassword",
        data={
            "current_password": "admin",
            "new_password": "adminnew",
            "new_password_repeat": "adminnew",
        },
        follow_redirects=True,
    )
    assert response.status_code == 200
    assert "<title>SARchive - Admin</title>" in response.get_data(as_text=True)
    assert "Your password was successfully changed." in response.get_data(as_text=True)


def test_change_password_wrong_password(client, login_ui):
    login_ui("admin", "admin")

    response = client.get("/admin/changepassword")
    assert response.status_code == 200
    assert "<title>SARchive - Change Password</title>" in response.get_data(
        as_text=True
    )

    response = client.post(
        "/admin/changepassword",
        data={
            "current_password": "adminwrong",
            "new_password": "adminnew",
            "new_password_repeat": "adminnew",
        },
        follow_redirects=True,
    )
    assert response.status_code == 200
    assert "<title>SARchive - Change Password</title>" in response.get_data(
        as_text=True
    )
    assert (
        "The current password you provided is incorrect. Please try again."
        in response.get_data(as_text=True)
    )
