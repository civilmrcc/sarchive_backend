from .test_data.generate_test_data import generate_full_test_data

tags_query = """
  tags {
    id
    name
  }
"""


def test_tags_return_object(run_gql, login, postgresql_db):
    generate_full_test_data()
    token = login("admin", "admin")
    result = run_gql(query=tags_query, token=token)["data"]

    assert result["tags"][0]["name"] == "other"
    assert result["tags"][1]["name"] == "another"
    assert result["tags"][2]["name"] == "test tag"
