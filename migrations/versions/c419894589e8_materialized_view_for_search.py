"""Materialized View for Search

Revision ID: c419894589e8
Revises: 61792d3faa52
Create Date: 2021-01-04 11:19:20.310521

"""
from alembic import op

# revision identifiers, used by Alembic.
revision = "c419894589e8"
down_revision = "61792d3faa52"
branch_labels = None
depends_on = None

# Extracted Strings for reuse in pytest
view_string = """CREATE MATERIALIZED VIEW full_cases_mv AS select c.id, c.case_number, c.freetext, c.occurred_at, b.boat_type, b.color, b.engine_status, t."name" as tag
    from cases as c left outer join boat_details as b on c.id = b.case_id
      left outer join tag_assignments as ta on c.id = ta.case_id
      left outer join tags t on ta.tag_id = t.id
    order by c.case_number;
"""

index_columns = "case_number || ' ' || freetext"
index_string = f"CREATE INDEX ix_full_cases_mv_fulltext ON full_cases_mv USING gin (to_tsvector('english', {index_columns}));"
index_unique = "CREATE UNIQUE INDEX ix_full_cases_mv_unique ON full_cases_mv (id, tag);"
index_occurred = "CREATE INDEX ix_full_cases_mv_dates ON full_cases_mv (occurred_at);"
index_boat_type = (
    "CREATE INDEX ix_full_cases_mv_boat ON full_cases_mv (boat_type, color);"
)


def upgrade():
    op.execute(view_string)
    op.execute(index_string)
    op.execute(index_unique)
    op.execute(index_occurred)
    op.execute(index_boat_type)


def downgrade():
    op.execute("DROP MATERIALIZED VIEW full_cases_mv")
