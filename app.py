import os

import click
from dotenv import load_dotenv
from flask_migrate import Migrate, upgrade

from cases_backend import create_app, db
from cases_backend.auth import User
from cases_backend.auth.role_model import Role
from cases_backend.models import (
    BoatDetails,
    Case,
    PeopleOnBoard,
    Position,
    Tag,
    FullCaseForSearch,
)

dotenv_path = os.path.join(os.path.dirname(__file__), ".env")
if os.path.exists(dotenv_path):
    load_dotenv(dotenv_path)


app = create_app(os.getenv("FLASK_CONFIG") or "default")
migrate = Migrate(app, db)


@app.shell_context_processor
def make_shell_context():
    return dict(
        db=db,
        User=User,
        Case=Case,
        BoatDetails=BoatDetails,
        PeopleOnBoard=PeopleOnBoard,
        Position=Position,
        Tag=Tag,
        Role=Role,
        FullCaseForSearch=FullCaseForSearch,
    )


@app.cli.command(help="Create sample cases with fake data.")
@click.option("--verbose", "-v", is_flag=True, help="Verbose output.")
@click.option(
    "--cases", "-c", type=int, default=10, help="Number of cases to be generated."
)
@click.option(
    "--init-db",
    is_flag=True,
    help="Initialize the database before the creation.",
)
@click.option(
    "--silent",
    is_flag=True,
    help="No Output.",
)
def create_sample_data(verbose, cases, init_db, silent):
    from cases_backend.db.generate_sample_data import generate_sample_data

    if init_db:
        _initialize_db()
    else:
        upgrade()

    generate_sample_data(verbose, cases, db, silent)
    FullCaseForSearch.update_materialized_view()


@app.cli.command(help="Initialize the database.")
def initialize_db():
    _initialize_db()


def _initialize_db():
    upgrade()

    Role.insert_default_roles()  # Need to be created before the user
    User.insert_default_user()
